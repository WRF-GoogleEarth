///////////////
// SLIDER
///////////////

/*
 * Logic for buttons on slider
 */
function buttonLogic(){
    $("button").button();

    $("#play-button").click(function(){
       // Makes sure that the process isn't already running
       if(timer == null){
          if($("#slider").slider("option", "value") == kmls.length - 1){
	     animate(0);
          }
          else{
             animate($("#slider").slider("option", "value") + 1);
          }
	}
    });

    $("#pause-button").click(function(){
       window.clearTimeout(timer);
       timer = null;
    });

    $("#stop-button").click(function(){
       kmls[$("#slider").slider("option", "value")].setMap(null);	    
       $("#slider").slider("option", "value", 0);
       window.clearTimeout(timer);
       timer = null;
    });
}

/*
 * Slider to control the flow of animation
 */
function initializeSlider(){
    buttonLogic();
    $("#slider").slider({
      value:0,
      min: 0,
      max: 0,
      step: 1,
      change: function(event, ui){
	if(ui.value > 0){
	    kmls[ui.value-1].setMap(null);	    
	}
	else{
	    kmls[kmls.length-1].setMap(null);	    
	}
	// Sets the next value to visible
	kmls[ui.value].setMap(map);	    
      },
      slide: function(event, ui){
        window.clearTimeout(timer);
	// Sets the previous kml to non-visible
	kmls[$("#slider").slider("option", "value")].setMap(null);
	
	// Sets the next value to visible
	kmls[ui.value].setMap(map);
        }
    });
}
  
///////////////
// ANIMATION 
///////////////

var kmls = [];
var duration;
var timer;

/*
 * called by the event listener after a click to start the animation
 */
function initiateFolderContents(location, folder, filename, cursor){
    kmls = [];

    // Initializes Slider
    initializeSlider();

    animateFolderContents(location, folder, filename, cursor);
}

/*
 * folder - directory files are saved in
 * cursor - number that has to be called
 */
function animateFolderContents(location, folder, filename, cursor){
    // Places a slash at the end of directory
    if (folder[folder.length-1] != "/"){
      folder += "/";
    }

    var cursorString = cursor + "/";

    // var numberString = createNumberString(cursor, 5);
    var image = folder + cursorString + filename;
    
    $.ajax({
      url:image,
      cache:false,
      success:function(){
        var mydomain="https://" + window.location.hostname + "/";
  
        var marker = new google.maps.KmlLayer(mydomain + image);

	// Disables auto focus
	marker.set("preserveViewport", true);

    kmls.push(marker);

	// Makes the image viewable if it is the first, and auto focuses in on it
	if (kmls.length == 1){
	    kmls[0].setMap(map);
	}

	// Sets everything back to frame 0 and allows you to play again
    kmls[$("#slider").slider("option", "value")].setMap(null);

	$("#slider").slider("option", "value", 0);
	$("#slider").slider("option", "max", kmls.length - 1);
        animateFolderContents(location, folder, filename, cursor + 1);
      },
      error:function(){
        // If image does not exist retry after 2 seconds
        setTimeout(function(){
           animateFolderContents(location, folder, filename, cursor);
        }, 2000);
      }
    });

}

/*
 * Sets the duration and starts the animation
 */
function startAnimation(duration){
    this.duration = duration;
    animate(0);
}

/*
 * LOGIC FOR ANIMATION called by startAnimation(...)
 * FrameByFrame - Boolean, stacks frames on top of one another if false
 */
function animate(slideNum){
    if (slideNum < kmls.length){
	$("#slider").slider("option", "value", slideNum);
	timer = window.setTimeout(function(){
	    animate(slideNum + 1);
        }, duration);
    }
    else{
	timer = null;
    }
}

/*
 * Places the animation on the screen, called by event listener
 */
function placeAnimation(location, pretext, startSlide, endSlide, format) {
	var marker;
        var boundaries;
	var numberString;
	for (var i=startSlide; i <= endSlide; i++){
		numberString = createNumberString(i, 5);
		boundaries = new google.maps.LatLngBounds(new google.maps.LatLng(location.lat()-.025, location.lng()-.025), new google.maps.LatLng(location.lat()+.025, location.lng()+.025));

		marker = new google.maps.GroundOverlay(pretext + numberString + "." + format, boundaries);
		kmls.push(marker);
	}
}
  
/*
 * Places an info marker with the latitude and longitude
 */
function placeInfoMarker(location) {
    var marker = new google.maps.Marker({
      position: location,
      map: map
    });

    var infowindow = new google.maps.InfoWindow({
      content: "Latitude: " + location.lat() + "<br/>Longitude: " + location.lng()
    });
    
    infowindow.open(map, marker);
}

/*
 * Creates a number string with preceding 0's specified by length
 */
function createNumberString(number, length){
	var string = "";
	for (var i = number.toString().length; i < length; i++){
		string += "0";
	}
	string += number.toString();
	return string;
}
