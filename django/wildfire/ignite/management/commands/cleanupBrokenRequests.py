"""
A management command which deletes expired accounts (e.g.,
accounts which signed up but never activated) from the database.

Calls ``Location.objects.handleBrokenRequest()``, which
contains the actual logic for determining which accounts are deleted.

"""

from django.core.management.base import NoArgsCommand
from ignite.models import Location


class Command(NoArgsCommand):
    help = "Handles requests that are more than 24 hours to compute."

    def handle_noargs(self, **options):
        Location.objects.handleBrokenRequest()
        print "cleanupBrokenRequests handled successfully"
