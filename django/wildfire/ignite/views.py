import thread, datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.conf import settings

from ignite.models import LocationForm, Location
from paramikoSSH import make_WRF_request


##########################################################
##########################################################
#
### HTTP RESPONSE CODES
#
# 200 - Success
#
##########################################################
##########################################################

# Calls the template that connects to Google Earth/Maps
@login_required
def requestFire(request):
    locations = Location.objects.filter(user=request.user)
    return render_to_response('ignite/request_fire_simulation.html', { 'locations': locations }, context_instance=RequestContext(request))

# This is the AJAX Call when clicking on the map that will show the request
# information.
@login_required
def regData(request):
    if request.method == 'POST':
        form = LocationForm(request.POST)

        if form.is_valid():

            data = form.save()

            # Starts the Paramiko request of making the files on the Server side
            if settings.DEBUG_PARAMIKO:
                make_WRF_request(data.pk, data)
            else:
                thread.start_new_thread(make_WRF_request, (data.pk, data))

            # This will go into the thread upon creation
            return render_to_response('ignite/requestConfirmation.html', { 'location': data }, context_instance=RequestContext(request))

    else:
        form = LocationForm(initial={'user':request.user.pk})
    
    return render_to_response('ignite/regData.html', { 'form': form, }, context_instance=RequestContext(request))


# Views all of the requests that the user has made
def viewRequests(request, user_id):
    locations = Location.objects.filter(user=user_id).order_by('title')
    return render_to_response('ignite/viewRequests.html', { 'locations': locations }, context_instance=RequestContext(request))

# Views KMZ that was put in
def viewKMZ(request, kmz_id):
    location = Location.objects.get(pk=kmz_id)
    expire_date = location.timestamp + datetime.timedelta(days=settings.WRF_REQUEST_EXPIRE_DAYS)
    return render_to_response('ignite/viewKMZ.html', { 'location': location, 'expire_date': expire_date }, context_instance=RequestContext(request))

# Renews the Timestamp to this current time
def renewTimestamp(request, kmz_id):
    Location.objects.get(pk=kmz_id).renew_timestamp()
    return HttpResponseRedirect("/viewKMZ/%s/" % kmz_id)
