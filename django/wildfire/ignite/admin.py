from ignite.models import Location
from django.contrib import admin

class LocationAdmin(admin.ModelAdmin):
    list_display = ('timestamp', 'user', 'title', 'is_complete', 'pk', 'ignTime', 'map_proj',)

admin.site.register(Location, LocationAdmin)


