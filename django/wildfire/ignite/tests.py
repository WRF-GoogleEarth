import socket, sys, os, inspect

from paramiko import SSHClient
from django.test import TestCase

# Returns the function that is being called
def function_title():
    return inspect.stack()[1][3]

class ParamikoTest(TestCase):
    def test_connection_failure(self):
        """
        Tests to make sure that socket knows if it fails to connect
        """

        try:
            self.ssh = SSHClient()
            self.ssh.load_system_host_keys()
            self.ssh.connect(hostname="gross.ucdenver.edu", username="mmartin", timeout=0.)
        except socket.error, e:
            print "Passed, Caught TimeOutError", e
        except:
            # Unkown Exception
            print sys.exc_info()

    def test_path_file(self):
        """
        See's if it can return the same file path
        """
        print function_title()
        print self.__class__.__name__

