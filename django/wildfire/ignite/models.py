import datetime

from django.conf import settings
from django.db import models
from django import forms
from django.contrib.auth.models import User
from ignite.paramikoSSH import email_user_error

class LocationManager(models.Manager):

    def handleBrokenRequest(self):
        """
        See's if something went wrong in the request process.
        If something goes wrong, it will notify the user
        """
        for location in self.all():
            if location.request_timeout():
                email_user_error(location)
                location.delete()

    def handleExpiredRequest(self):
        """
        Removes the request if it has expired
        """
        for location in self.all():
            if location.location_expired():
                location.delete()


class Location(models.Model):
    """
    Profile that stores the location.
    """

    user = models.ForeignKey(User)
    title = models.CharField(max_length=30)

    objects = LocationManager()

    map_proj_choices = (('la', 'lambert'),
                        ('me', 'mercator'),
                        ('po', 'polar'),
                        ('rl', 'rotated_ll'),
                        ('ll', 'lat-lon'))

    centerLat    = models.FloatField()
    centerLon    = models.FloatField()
    ignLat       = models.FloatField()
    ignLon       = models.FloatField()
    ignTime      = models.DateTimeField("Ignition Time")
    dx           = models.FloatField(default=100)
    dy           = models.FloatField(default=100)
    nx           = models.IntegerField(default=40)
    ny           = models.IntegerField(default=41)
    runTime      = models.FloatField("Run Time", default=1)
    spinupTime   = models.FloatField("Spinup Time", default=.001)
    map_proj     = models.CharField("Map Projection", max_length=2, choices=map_proj_choices, default='la')
    ign_radius   = models.FloatField("Ignition Radius", default=10)
    is_complete  = models.BooleanField("Completed", default=False, editable=False)
    timestamp    = models.DateTimeField("Time Stamp", auto_now=True)

    def attrs(self):
        """
        Iterates through the keys so you know what is
        within the particular model.
        """
        for attr, value in self.__dict__.iteritems():
            yield attr, value

    def request_timeout(self):
        """
        See's if the request has been taking too long.
        """
        expiration = datetime.timedelta(hours=settings.WRF_REQUEST_TIMEOUT_HOURS)
        return (self.timestamp + expiration <= datetime.datetime.today())

    def location_expired(self):
        """
        Looks at if location has exceeded max number of days
        """
        expiration = datetime.timedelta(days=settings.WRF_REQUEST_EXPIRE_DAYS)
        return (self.timestamp + expiration <= datetime.datetime.today())

    def renew_timestamp(self):
        """
        Renews the timestamp so it doesn't expire another 7 days
        """
        self.timestamp = datetime.datetime.today()
        self.save()

    def __unicode__(self):
        return str(self.pk) + ' - ' + self.title

class LocationForm(forms.ModelForm):
    """
    Form that will be generated when requested by the user
    """
    user = forms.ModelChoiceField(widget=forms.HiddenInput(), queryset=User.objects.all())
    centerLat   = forms.FloatField(widget=forms.HiddenInput())
    centerLon   = forms.FloatField(widget=forms.HiddenInput())
    ignLat      = forms.FloatField(widget=forms.HiddenInput())
    ignLon      = forms.FloatField(widget=forms.HiddenInput())
    title       = forms.CharField(help_text="Used for reference")
    ignTime     = forms.DateTimeField(initial='10/5/2011 5:00:00', label="Ignition Time")

    class Meta:
        model = Location
