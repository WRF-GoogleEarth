from django.conf import settings
from django.contrib.sites.models import Site
from django.template import loader, Context
from django.core.mail import send_mail
from django.core.mail import mail_managers

import paramiko, os, socket, inspect


## Prints the title of the function that threw the error
def function_title():
    return inspect.stack()[1][3]

class WRF_Request():
    '''
    Does the Request The user made on the Server Side

    Be sure that the Server has your id_rsa.pub inside of its authorized_keys file
    under the .ssh/ directory.  This will prevent you from having to enter
    a password.
    '''

    # Host of the Server you are connecting to
    host = settings.KMZ_REQUEST_HOST

    # The username to log in as
    user = settings.KMZ_REQUEST_USERNAME

    # The path the Server sends the generated files back to
    uploadpath = settings.KMZ_UPLOAD_SCP_PATH

    # The directory to create the wrf files
    src_directory = '/tmp/wrf/'
    des_directory = '/tmp/wrf/'

    def __init__(self, session, form_data):

        self.session = session

        self.ssh = paramiko.SSHClient()
        # Loads the host keys necessary for connection
        self.ssh.load_system_host_keys()

        self.ssh.connect(hostname=self.host, username=self.user, timeout=20.)

        print "Starting Session " + str(session)

        filename = self.createParamsTextFile(session, form_data)
        self.params_file = filename

        self.createMediaPath(session)

    # Creates the return paths
    def createMediaPath(self, session_id):
        path = settings.MEDIA_ROOT + '/'

        # See's if the directory already exists
        if os.path.exists(path + str(session_id)):
            print "Directory already exists"
            mail_managers("Wildfire Error", "The Session under " + settings.MEDIA_ROOT + " already exists and there was an error parsing the data.  The directory was " + path + str(session_id) + ".", fail_silently=settings.EMAIL_FAIL_SILENTLY)
        else:
            os.mkdir(path + str(session_id))

        os.chmod(path + str(session_id), 01775)

        print "Now changing permissions"

        # 30 is for wwwrun user
        # 8 is for www group
        for root, dirs, files in os.walk(path + str(session_id)):
            for momo in dirs:
                os.chown(os.path.join(root, momo), 30, 8)
            for momo in files:
                os.chown(os.path.join(root, momo), 30, 8)

        print "done"

    # Creates the text file containing parameters
    def createParamsTextFile(self, session_id, form):
        filename = "parms_%i.txt" % session_id
        f = open(self.src_directory + filename, "w")

        self.uploadpath += str(session_id) + '/'

        data = self.parseForm(form)
        for key, value in data.iteritems():
            f.write(str(key) + "=" + str(value) + "\n")

        f.close()

        return filename


    # Get's the session number
    def get_session(self):
        return self.session

    def ssh_execute_fire_simulation(self):
        # Destination on this client to bring files back to
        cmd = '/opt/lib/epd/bin/python /storage/fire/server/fireSim.py '
        cmd += str(self.session) + ' '
        cmd += self.des_directory + self.params_file

        stdin, stdout, stderr = self.ssh.exec_command(cmd)

        print stdout.read()

    # Transfer the temporary file to the server for execution
    def transfer_params_file(self):
        sftp = paramiko.SFTPClient.from_transport(self.ssh.get_transport())
        sftp.put(self.src_directory + self.params_file, self.des_directory + self.params_file)

        sftp.close()

    # Parse form for WRF and saves it to a file
    def parseForm(self, form):

        # Creates Dictionary
        data={}

        # Makes the dictionary that will store the values
        for key, value in form.attrs():
            if key[0] != '_':
                if key != 'id':
                    data[key]=value

        # Changes the 'key' for map_proj to value
        map_key = data['map_proj']
        for tuple in form.map_proj_choices:
            if tuple[0] == map_key:
                data['map_proj'] = tuple[1]
                break

        # Changes time to be the correct formatting
        time = str(data['ignTime'])
        time = time.replace(" ", "_")
        data['ignTime'] = time

        data['uploadpath'] = self.uploadpath

        return data

    def close_shell(self):
        self.ssh.close()
        print "Session %i Complete" % self.session

# Called by the view to send the email upon completion
def make_WRF_request(session, data):
    try:
        w = WRF_Request(session, data)
        w.transfer_params_file()
        w.ssh_execute_fire_simulation()
        w.close_shell()

        # After completion of the shell, it will execute these statements
        data.is_complete = True
        data.save()
        email_user_success(data, session)

    except socket.error, e:
        # An error happened when connecting to another server
        user = data.user

        site = Site.objects.get(id=settings.SITE_ID).domain

        t_managers = loader.get_template('email/notify_socket_error_mangrs.txt')
        c = Context({
            'site_name'   : site,
            'query_title' : data.title,
            'user_email'  : user.email,
            'error'       : e,
        })

        # Sends email regarding malfunction
        email_user_error(data)
        mail_managers(site + " Urgent Notice WRF Request Socket Down", t_managers.render(c), fail_silently=settings.EMAIL_FAIL_SILENTLY)
        data.delete()

    except:
        # Unknown Error Has Occured
        user = data.user

        site = Site.objects.get(id=settings.SITE_ID).domain

        t_managers = loader.get_template('email/notify_socket_error_mangrs.txt')
        c = Context({
            'site_name'     : site,
            'query_title'   : data.title,
            'user_email'    : user.email,
            'file_path'     : os.path.realpath(__file__),
            'function_call' : function_title(),
        })

        # Sends email regarding malfunction
        email_user_error(data)
        mail_managers(site + " Urgent Notice WRF Unknown Exception", t_managers.render(c), fail_silently=settings.EMAIL_FAIL_SILENTLY)
        data.delete()

# Emails the default error email telling user that his/her request didn't work properly
def email_user_error(data):
    user = data.user

    site = Site.objects.get(id=settings.SITE_ID).domain

    t    = loader.get_template('email/notify_error_user.txt')

    c = Context({
        'site_name'     : site,
        'query_title'   : data.title,
    })


    send_mail(site + ' Query Malfunction', t.render(c), settings.DEFAULT_FROM_EMAIL,[user.email], fail_silently=settings.EMAIL_FAIL_SILENTLY)

# Emails the user telling him of completion of his query
def email_user_success(data, session):
    
    user = data.user

    site = Site.objects.get(id=settings.SITE_ID).domain

    t = loader.get_template('email/notify_WRF_completion.txt')
    c = Context({
        'site_name': site,
        'query_path': site + '/viewKMZ/' + str(session) + '/',
        'query_title': data.title,
    })

    send_mail(site + ' Query Complete', t.render(c), settings.DEFAULT_FROM_EMAIL,[user.email], fail_silently=settings.EMAIL_FAIL_SILENTLY)
