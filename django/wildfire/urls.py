from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'ignite.views.requestFire', name='home'),
    url(r'^regData/$', 'ignite.views.regData'),
    url(r'^viewRequests/(?P<user_id>\d+)/$', 'ignite.views.viewRequests'),
    url(r'^viewKMZ/(?P<kmz_id>\d+)/$', 'ignite.views.viewKMZ'),
    url(r'^viewKMZ/(?P<kmz_id>\d+)/renewTimestamp/$', 'ignite.views.renewTimestamp'),
    url(r'^accounts/', include('registration.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        })
    )

urlpatterns += staticfiles_urlpatterns()
