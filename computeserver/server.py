#!/usr/bin/env python

# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.
import random, sys

from twisted.spread import pb
from twisted.internet import reactor

import computeDaemon

# for quick testing of server infrastructure, uncomment:
computeDaemon.startSession=computeDaemon.startTestSession

users = {}

class WRF_Handler(pb.Root):

    # Sends the Client Class for calls back to the client
    # takes a dictionary of simulation parameters.
    # see README.params.txt for a list of keywords.
    def remote_startRequest(self, client, params):
        print "received user", client
        id = random.randint(1, sys.maxint)

        # Makes sure id isn't already taken
        while users.has_key(id):
            id = random.randint(1, sys.maxint)

        users[id] = client
        
        # get a callback function suitable for passing to visualization process
        callback=lambda variable,imageData : client.callRemote("sendImage",variable,imageData)

        # start simulation process
        computeDaemon.startSession(id,params,callback=callback)
        
        print "telling it to print(%i)" % id
        client.callRemote("sessionID", id)

    # Checks the status of the request
    def remote_status(self,id):
        s=getSession(id)
        stat=s.status()
        print 'sending status of %s: %s' % (str(id),stat)
        return stat

    # Cancel the request
    def remote_cancel(self,id):
        print 'canceling %s' % str(id)
        s=getSession(id)
        s.kill()

reactor.listenTCP(8800, pb.PBServerFactory(WRF_Handler()))
reactor.run()
