
defaultParams={
"centerLat":40.07160929510595,
"centerLon":-105.9405756939366,
"ign_radius":10,
"dx":100,
"dy":100,
"nx":40,
"ny":41,
"ignLat":40.07160929510595,
"ignLon":-105.9405756939366,
"ignTime":"2011-10-05_00:01:00",
"runTime":1,
"spinupTime":.001,
"map_proj":"lambert",
"uploadpath":""
}

def writeParams(file,**params):
    d=defaultParams.copy()
    d.update(params)
    lines=[ '%s=%s'%( str(p),str(v) ) for p,v in d.iteritems() ]
    file.write('\n'.join(lines))

def readParams(file):
    d=defaultParams.copy()
    lines=[ l.split('=') for l in file.readlines() ]
    params={ p.strip():v.strip() for p,v in lines }
    d.update(params)
    return d 
