#!/usr/bin/env python

# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.

from twisted.spread import pb
from twisted.internet import reactor

class Client(pb.Referenceable):

    def remote_sessionID(self, arg):
        print "Two.print() called with", arg

    #def remote_complete(self, arg):
    #    print "Complete with #images = ", arg

    def remote_sendImage(self,variable,imageData):
        print 'Got kmz for variable %s, size %i bytes' % (variable,len(imageData))

def main():
    client = Client()
    factory = pb.PBClientFactory()
    reactor.connectTCP("localhost", 8800, factory)

    def1 = factory.getRootObject() # gets the root object which is WRF_Handler
    def1.addCallback(got_obj, client) # hands our 'two' to the callback
    reactor.run()

def got_obj(obj, client):
    print "got WRF_HANDLER:", obj
    print "giving it our two"
    obj.callRemote("startRequest", client, {})
    #obj.callRemote('status')

main()
