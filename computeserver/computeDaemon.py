#!/usr/bin/env python
'''
Server class for the fire simulation backend.  Includes a test class which
emulates the actual server without needing to set up any of the configuration
files or dependent modules.
'''

from threading import Thread,RLock
import multiprocessing
import os
import shutil
import time
from glob import glob

# for the test class:

# how long to sleep (in seconds) between actions
testsleepinterval=10

# directory containing a series of kmz files from a real run...
# (included in the root of the remoteFire repository)
testkmzdirectory=os.path.abspath(os.path.join(os.path.curdir,'kmz'))

sessions={}

class fireSession(object):
    '''
    Fire simulation server instance.  This class tarts up a session in the 
    background which can be controlled and queried through class methods.
    '''
    def __init__(self,sessionID,params,callback=None):
        '''
        Initialize class instance.  
        sessionID: any unique string identifier for a session
        params:    parameter file (or string) controlling the location and 
                   size of simulation (among other things).  See params.txt.
        '''

        # add imports here to allow test class without dependencies
        from globalConfig import get
        from fireSim import run
        self.ID=sessionID
        self.params=params

        # simulation run directory
        self.runDir=os.path.join(get('server','mainDir'),str(self.ID))

        # simulation kmz output directory
        self.KMZDir=os.path.join(self.runDir,'kmz')

        self._openSession(run,(self.ID,self.params,self.runDir,callback))

    def _openSession(self,target,args):
        '''
        Open a fire session in a background process.
        '''
        self._thread=multiprocessing.Process(target=target,args=args)
        self._thread.start()

    def running(self):
        '''
        Returns true if simulation is running or false if not.
        '''
        return self._thread.is_alive()

    def status(self):
        '''
        Returns a status message that can be displayed to the user regarding
        what the process is currently doing.  This is currently a place holder
        for now until more detailed status messages are implemented.
        '''
        if self.running():
            return 'Running'
        else:
            return 'Not Running'

    def kill(self):
        '''
        Stop the fire session.
        '''
        if self.running():
            self._thread.terminate()
            self._thread.join()
        return self._thread.exitcode

    def clean(self):
        '''
        Delete all files from the session.  (Including output kmz files)
        '''
        try:
            shutil.rmtree(self.runDir)
        except OSError:
            pass

    def getFiles(self):
        '''
        Get a list of all kmz output files created so far.  Note: these files
        are created as the simulation procedes, so you can poll this method
        while running to see if any new files have been created.
        '''
        files=[]
        for f in glob(os.path.join(self.KMZDir,'.status*')):
            files.extend(open(f,'r').readlines())
        return [ os.path.join(self.KMZDir,f) for f in files ]

class testSession(fireSession):
    '''
    A test class which emulates the fireSession class without actually doing anything
    or requiring any dependent modules or other setup.  Good for local testing of the 
    web application.
    '''

    def __init__(self,sessionID,params,callback=None):
        '''
        See fireSession.__init__
        (params can be anything here, as it is not used)
        '''
        self.ID=sessionID
        self.params=params
        self.runDir=os.path.join(os.path.abspath('run'),str(self.ID))
        self.KMZDir=os.path.join(self.runDir,'kmz')
        self._openSession(self._testRun,(callback,))

    def _testRun(self,callback=None):
        '''
        Emulation of a fire session.  This is run in a background process, where it
        copies kmz files into the output directory with a delay.
        '''
        try:
            shutil.rmtree(self.runDir)
        except OSError:
            pass
        os.makedirs(self.runDir)
        os.makedirs(self.KMZDir)

        time.sleep(testsleepinterval)
        statfile=os.path.join(self.KMZDir,'.status_fgrnhfx')
        for z in glob(os.path.join(testkmzdirectory,'*.kmz')):
            stat=open(statfile,'a')
            shutil.copy(z,self.KMZDir)
            stat.write(os.path.basename(z)+'\n')
            stat.close()
            if not callback is None:
                callback('fgrnhfx',open(z,'r').read())
            time.sleep(testsleepinterval)

class InvalidSessionID(Exception):
    def __str__(self):
        return "Invalid SessionID: %s" % str(self.args[0])

class DuplicateSessionID(Exception):
    def __str__(self):
        return "SessionID %s already exists" % str(self.args[0])

def getSession(sessionID):
    '''
    Get a session instance for the given sessionID.  Must already have been started using
    startSession.
    '''
    if sessions.has_key(sessionID):
        return sessions[sessionID]
    else:
        raise InvalidSessionID(sessionID)

def startSession(sessionID,params,callback=None):
    '''
    Start a fire session for the given parameters.  The sessionID must be unique.
    Takes simulation parameters and a callback function (optional). 
    If callback function is present, then call this function with
    two arguments (variable name, kmz data) for each kmz file created 
    (ordered by time created).
    '''
    if sessions.has_key(sessionID):
        raise DuplicateSessionID(sessionID)
    sessions[sessionID]=fireSession(sessionID,params,callback)
    return sessions[sessionID]

def startTestSession(sessionID,params,callback=None):
    '''
    Start a test session instance.
    '''
    if sessions.has_key(sessionID):
        raise DuplicateSessionID(sessionID)
    sessions[sessionID]=testSession(sessionID,params,callback)
    return sessions[sessionID]

# Just run a test session to check that the results are sane.
if __name__ == '__main__':
    print 'starting session "aaa"'
    s=testSession('aaa','')
    print 'status of "aaa": %s' % s.status()
    print 'starting session "bbb"'
    t=testSession('bbb','')
    print 'status of "bbb": %s' % s.status()

    time.sleep(30)
    print 'completed files for session "aaa":'
    print '\n'.join(s.getFiles())
    print 'killing session "aaa"'
    s.kill()

    print 'completed files for session "bbb":'
    print '\n'.join(t.getFiles())
    
    print 'status of "aaa": %s' % s.status()
    print 'status of "bbb": %s' % t.status()
    
    time.sleep(30)

    print 'completed files for session "aaa":'
    print '\n'.join(s.getFiles())
    print 'completed files for session "bbb":'
    print '\n'.join(t.getFiles())
    
    print 'status of "aaa": %s' % s.status()
    print 'status of "bbb": %s' % t.status()

    print 'cleaning up session "aaa"'
    s.clean()

    print 'waiting for "bbb" to finish'



