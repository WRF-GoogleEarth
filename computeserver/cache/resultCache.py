#!/usr/bin/env python

#from shelve import Cache as _Cache
from functools import wraps
import hashlib
import os
import string
import re
import sys

from percache import Cache as perCache

from globalConfig import get as _get

class NotCachedError(Exception):
    pass

class Cache(perCache):
    '''
    Same as percache.Cache except using project defaults.
    '''
    def __init__(self,cachefile=None,livesync=None):
        if cachefile is None:
            cachefile=_get('cache','cachefile')
#        if repr is None:
#            repr=self.__cacherep__
        if livesync is None:
            livesync=_get('cache','livesync')
        perCache.__init__(self,cachefile,livesync=livesync)


class FileCache(object):

    _translatechars=string.maketrans(' ','_')
    _validchars=re.compile(r'[^0-9a-zA-Z_\-.:()]*')
    _hexLen=48
    _files='.files'

    def __init__(self,fileStore=None,runDir='.',repr=repr):
        if fileStore is None:
            fileStore=_get('cache','fileStore')
        self._fileStore=fileStore
        self._runDir=runDir
        self._repr=repr

    @classmethod
    def toDirs(cls,a):
        d=[]
        for b in a:
            if b:
                d.append(cls.toFileString(b))
        return os.path.join(*d)

    @classmethod
    def toFileString(cls,s):
        '''Convert an object into something that we can use as a file/dir name'''
        s=str(s)
        args=s
        assert s
        s=s.translate(cls._translatechars)
        s=cls._validchars.sub('',s)
        s=s.lstrip('.-')
        if len(s) == 0 or len(s) > cls._hexLen:
            s=cls.toHex(args)
        return s

    @staticmethod
    def toHex(s):
        assert isinstance(s,basestring)
        c=hashlib.sha1(s)
        return c.hexdigest()

    def getDir(self,args):
        d=[self._filestore]
        for a in args:
            d.append(self.toFileString(a))

    @staticmethod
    def linkFile(ffrom,fto,linkcmd=os.symlink):
        ofrom=ffrom
        if linkcmd is os.symlink:
            cmd='symlink'
        else:
            cmd='move'
            ffrom=os.path.realpath(ffrom)

        if os.path.isdir(fto):
            fto=os.path.join(fto,os.path.basename(ofrom))
        try:
            os.remove(fto)
        except Exception:
            pass
        try:
            os.makedirs(os.path.dirname(fto))
        except Exception:
            pass
        ffrom=os.path.abspath(ffrom)
        print >> sys.stderr, '%s: %s => %s' % (cmd,ffrom,fto)
        linkcmd(ffrom,fto)

    def getCache(self,func,args,kwargs):
        oargs=args[:]
        assert len(args) > 0
        a=[]
        for o in args:
            if not isinstance(o,basestring):
                o=self._repr(o).split(os.path.sep)
            else:
                o=o.split(os.path.sep)
            a.extend(o)
        args=a
        d=os.path.join(self._fileStore,self.toDirs(args))
        f=os.path.join(d,self._files)
        print >> sys.stderr, "Raw cache args %s" % (str(args))
        print >> sys.stderr, "Looking for cached files in %s" % f
        if os.path.exists(f):
            try:
                files=[]
                for g in open(f,'r').readlines():
                    g=g.strip()
                    if g:
                        self.linkFile(os.path.join(d,os.path.basename(g)),
                                      os.path.join(self._runDir,g))
                        files.append(g)
                print >> sys.stderr, "Using cached results in %s" % d
                return files
            except Exception as e:
                print >> sys.stderr, "In getCache, got exception %s" % str(e)
                pass
        return None

    def __call__(self,func):
        
        @wraps(func)
        def wrapper(*args, **kwargs):
            oargs=args[:]
            assert len(args) > 0
            a=[]
            for o in args:
                if not isinstance(o,basestring):
                    o=self._repr(o).split(os.path.sep)
                else:
                    o=o.split(os.path.sep)
                a.extend(o)
            args=a
            d=os.path.join(self._fileStore,self.toDirs(args))
            f=os.path.join(d,self._files)
            files=self.getCache(func,oargs,kwargs)
            if not files is None:
                return files
            print >> sys.stderr, "Recomputing %s" % f
            try:
                os.makedirs(os.path.dirname(f))
            except Exception:
                pass
            print >> sys.stderr, "Computing %s" % d
            files=func(*oargs,**kwargs)
            dotfiles=open(f,'w')
            for g in files:
                assert isinstance(g,basestring)
                self.linkFile(g,d,linkcmd=os.renames)
                self.linkFile(os.path.join(d,os.path.basename(g)),
                              os.path.join(self._runDir,g))
    
                dotfiles.write(g+'\n')
            dotfiles.close()
	    return files

        return wrapper

