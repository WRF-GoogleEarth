#!/usr/bin/env python
'''
Functions for downloading NARR atmospheric data sets from nomads.ncdc.noaa.gov
'''

import os
import sys
from datetime import datetime
import subprocess as sp

from urllib import urlretrieve
from ESMFDateTime import parse,ESMFDateTime
from datetime import timedelta

from cache import resultCache

def _GRIBrepr(a):
    if isinstance(a,dict):
        s=[ "%s%s" % (str(a),str(a[v])) for v in sorted(a) ]
        return os.path.sep.join(s)
    elif isinstance(a,datetime):
        return str(a)
    else:
        return getattr(a,'_clsname',repr(a))

_c=resultCache.FileCache(repr=_GRIBrepr)

class downloadGRIB(object):
    
    _url=''
    _data={}
    _file='%(year)04i%(month)02i%(day)02i_%(hour)02i%(minute)02i%(second)02i'
    _ext='grb'
    _interval=6
    _Vtable=''
    _clsname="GRIBbaseclass"
   
    @classmethod
    def parseDate(cls,date):
        if isinstance(date,str):
            date=parse(date)
        if isinstance(date,dict):
            return date.copy()
        d={}
        d['year']=date.year
        d['month']=date.month
        d['day']=date.day
        d['hour']=date.hour
        d['minute']=date.minute
        d['second']=date.second
        return d

    @classmethod
    def getURLString(cls,date,data={}):
        tmpdata=cls._data.copy()
        tmpdata.update(cls.parseDate(date))
        tmpdata.update(data)
        return cls._url % tmpdata

    @classmethod
    def getURL(cls,url,localfile):
        return urlretrieve(url,localfile)
    
    @classmethod
    def getFileName(cls,date,data={}):
        date=cls.parseDate(date)
        date.update(data)
        return (cls._file + '.' + cls._ext) % date
    
    @classmethod
    @_c
    def getFile(cls,date,data={}):
        url=cls.getURLString(date,data)
        print 'downloading GRIB %s: %s' % (date,url)
        file=cls.getFileName(date,data)
        return (cls.getURL(url,file)[0],)

    @classmethod
    def interval_seconds(cls):
        return cls._interval*60*60

    @classmethod
    def hoursValid(cls):
        n=24/cls._interval
        if cls._interval*n != 24 or not isinstance(cls._interval,int):
            raise Exception("Unimplemented hours method in %s." % str(cls))
        return range(0,24,cls._interval)

    @classmethod
    def getNearestBefore(cls,date):
        target=0
        for h in cls.hoursValid():
            if h <= date.hour:
                target=h
            else:
                break
        return ESMFDateTime(year=date.year,month=date.month,day=date.day,hour=target)

    @classmethod
    def getNearestAfter(cls,date):
        day=date.day
        for h in cls.hoursValid():
            if h >= date.hour:
                target=h
                break
        else:
            day=day+1
            target=0
        return ESMFDateTime(year=date.year,month=date.month,day=day,hour=target)

    @classmethod
    def getDateRange(cls,startDate,endDate):
        if isinstance(endDate,datetime):
            endDate=endDate-startDate
        elif isinstance(endDate,(int,float)):
            endDate=timedelta(hours=endDate)
        deltaT=timedelta(hours=cls._interval)
        startDate=cls.getNearestBefore(startDate)
        curDate=startDate
        dates=[]
        while curDate <= startDate + endDate:
            dates.append(curDate)
            curDate=curDate+deltaT
        return dates

    @classmethod
    def getFileRange(cls,startDate,endDate,data={}):
        dates=cls.getDateRange(startDate,endDate)
        files=[]
        for d in dates:
            files.extend(cls.getFile(d,data))
        return files

    @classmethod
    def getVtable(cls):
        return cls._Vtable

    @classmethod
    def linkGrib(cls,targetDir,files):
        targetDir=os.path.abspath(targetDir)
        exe=os.path.join(targetDir,'link_grib.csh')
        files=[ os.path.abspath(f) for f in files ]
        p=sp.Popen(args=' '.join([exe,]+files),shell=True,cwd=targetDir)
        p.communicate()
        vtable=cls.getVtable()
        try:
            os.unlink(os.path.join(targetDir,'Vtable'))
        except OSError:
            pass
        os.symlink(os.path.join(targetDir,'ungrib','Variable_Tables','Vtable.' + vtable),
                   os.path.join(targetDir,'Vtable'))

class downloadNARR(downloadGRIB):
    # example url:
    # http://nomads.ncdc.noaa.gov/data/narr/201110/20111005/narr-a_221_20111005_0000_000.grb
    _url='http://nomads.ncdc.noaa.gov/data/%(data1)s/%(year)04i%(month)02i/%(year)04i%(month)02i%(day)02i/%(data2)s_%(grid)s_%(year)04i%(month)02i%(day)02i_%(hour)02i%(minute)02i_%(data3)s.%(ext)s'
    _data={'data1':'narr','data2':'narr-a','data3':'000','grid':'221','ext':'grb'}
    _interval=3
    _Vtable='NARR'
    _clsname='nomads.ncdc.noaa.gov/narr'

gribSources={'NARR':downloadNARR}

if __name__ == '__main__':
    print 'try 1:'
    print downloadNARR.getFile(parse('2011-10-05_00:00:00'))
    print 'try 2:'
    print downloadNARR.getFile(parse('2011-10-05_00:00:00'))
