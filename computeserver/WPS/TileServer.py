
import os
import urllib2
from zipfile import ZipFile
import subprocess as sp
from math import ceil

from cache import resultCache

_fnamefmt='n%(lat)iw%(lon)i.zip'
_urlfmt='http://gisdata.usgs.gov/TDDS/DownloadFile.php?TYPE=ned3g_zip&ORIG=TNM&FNAME=%s'

def _geoRepr(a):
    return str(a)

_cache=resultCache.FileCache(repr=_geoRepr)

@_cache
def downloadTile(lon,lat):
    ilon,ilat=getTile(lon,lat)
    fname=_fnamefmt % {'lat':ilat,'lon':ilon}
    url=_urlfmt % fname
    print 'Downloading %s' % url
    u=urllib2.urlopen(url)
    f=open(fname,'w')
    f.write(u.read())
    f.close()
    
    z=ZipFile(fname)
    z.extractall()
    zdir=''
    for n in z.namelist():
        print 'checking %s for adf extension' % n
        if n[-3:] == 'adf':
            zdir=os.path.dirname(n)
            break
    
    print 'using directory %s' % zdir
    if not os.path.isdir(zdir):
        raise Exception('Unexpected format of zipfile download from USGS tile server.')
    
    gtiffname=fname[:-3] + 'tif'
    p=sp.Popen( ('gdal_translate','-of','GTiff',zdir,gtiffname) )
    p.communicate()
    
    if p.returncode:
        raise Exception('Conversion to GeoTIFF failed.')

    return (gtiffname,)

def downloadTileRange(bds):
    lonmin,lonmax,latmin,latmax=getTileRange(bds)

    tiles=[]
    for ilon in xrange(lonmin,lonmax+1):
        for ilat in xrange(latmin,latmax+1):
            tiles.append(downloadTile(ilon,ilat)[0])
    return mergeTiles(*tiles)

def mergeTiles(*files):
    if len(files) == 1:
        return files
    elif len(files) <= 0:
        return None
    
    p=sp.Popen( ('gdal_merge.py','-o','merged.tif','-of','GTiff') + tuple(files) )
    p.communicate()

    if p.returncode:
        raise Exception('Error merging geotiff tiles.')

    return ('merged.tif',)

def getTile(lon,lat):
    if abs(lon) > 125 or abs(lon) < 65 or lat < 25 or lat > 50:
        raise Exception('Coordinates outside of the CONUS.')
    ilon=ceil(abs(lon))
    ilat=ceil(abs(lat))
    return int(ilon),int(ilat)

def getTileRange(bds):
    lonmin,latmin=getTile(bds.lonmin,bds.latmin)
    lonmax,latmax=getTile(bds.lonmax,bds.latmax)
    assert latmin <= latmax and lonmin <= lonmax
    return lonmin,lonmax,latmin,latmax

def getNumTiles(bds):
    lonmin,lonmax,latmin,latmax=getTileRange(bds)
    return (lonmax - lonmin + 1) * (latmax - latmin + 1)

if __name__ == '__main__':
    from SeamlessServer import LatLonBounds

    bds=LatLonBounds(41.2,39.9,-119.9,-119.2)
    print 'Downloading %i tiles' % getNumTiles(bds)
    files=downloadTileRange(bds)

