#!/usr/bin/env python

import os
import shutil

from SourceDataDef import AllData
from SeamlessServer import LatLonBounds,SeamlessServer,setHTTPDebug
from TileServer import downloadTileRange
from cache import resultCache
import GeogridTable

def _geoRepr(a):
    return str(a)

_cache=resultCache.FileCache(repr=_geoRepr)
_data=AllData()
_metersperdegree=111131.0
_expandfactor=.5

@_cache
def getData(src,bounds):
    assert isinstance(bounds,LatLonBounds)
    assert _data.has_key(src)
    
    datadef=_data[src]
    if src == 'ned3':
        data=downloadTileRange(bounds)
    else:
        dataserv=datadef.getServer()
        data=dataserv.getData(datadef.getProductCode(),bounds)
    data=data[0]
    index=createIndex(os.path.dirname(data),datadef,data)
    return [data,index]

def createIndex(datapath,src,datafile):
    datafile0=os.path.basename(datafile)
    if not datapath == os.path.dirname(datafile):
        shutil.rmtree(datapath,ignore_errors=True)
        os.mkdirs(datapath)
        os.symlink(os.path.abspath(datafile),os.path.join(datapath,datafile0))
    indexfile=os.path.join(datapath,'index')
    index=open(indexfile,'w')
    index.write('geotiff=%s\n' % datafile0)
    index.write('description="%s"\n' % src.description)
    index.write('units="%s"\n'  % src.units)
    index.write('tile_x=%i\n'   % src.tilesize)
    index.write('tile_y=%i\n'   % src.tilesize)
    if src.iscategorical:
        index.write('type=categorical\n')
        index.write('category_min=1\n')
        index.write('category_max=%i\n' % src.numCats)
    else:
        index.write('type=continuous\n')
    index.write('missing_value=%f\n' % src.missingval)
    index.close()
    return indexfile
    
def fixTable(tblpath,datapath,src):
    if isinstance(src,basestring):
        src=_data[src]
    tbl=GeogridTable.parseTBL(tblpath)
    assert tbl.has_key(src.getDestVariable())
    v=tbl[src.getDestVariable()]
    v['halt_on_missing']=['no']
    v['abs_path']=['default:%s' % os.path.abspath(datapath)]
    GeogridTable.writeTBL(tbl,tblpath)

def getBounds(centerLon,centerLat,dx,dy,nx,ny):
    lonmin=centerLon - (1+_expandfactor)*nx*dx/(2. * _metersperdegree)
    latmin=centerLat- (1+_expandfactor)*ny*dy/(2. * _metersperdegree)
    lonmax=lonmin + (1+_expandfactor)*nx*dx/(_metersperdegree)
    latmax=latmin + (1+_expandfactor)*ny*dy/(_metersperdegree)
    return LatLonBounds(latmax,latmin,lonmax,lonmin)

def test():
    bounds=LatLonBounds(40.07160929510595,40.011401113951685,-105.9405756939366,-106.0093850438272)
    

    print "try 1:"
    print getData('ned3',bounds)
    print getData('anderson13',bounds)

    print "try 2:"
    print getData('ned3',bounds)
    print getData('anderson13',bounds)

if __name__ == '__main__':
    setHTTPDebug()
    test()
