#!/usr/bin/env python

'''For parsing GEOGRID.TBL and similar parameter files'''

import re
import os
import cStringIO

groupsplit=re.compile(r'====*')
comment=re.compile(r'#.*')
whitespace=re.compile(r'\s*')

class Table(dict):

    def __str__(self):
        s=cStringIO.StringIO()
        for g in sorted(self.keys()):
            s.write('='*16+'\n')
            s.write('name=%s\n' % g)

            for p in sorted(self[g].keys()):
                for i in range(len(self[g][p])):
                    s.write('    %s=%s\n' % (p,str(self[g][p][i])))
        s.write('='*16 + '\n')
        return s.getvalue()

def commentstrip(s):
    s=comment.sub('',s)
    return s.strip()

def parseTBL(f):
    if isinstance(f,basestring) and os.path.exists(f):
        f=open(f,'r')
    if isinstance(f,file):
        f=f.read()

    assert isinstance(f,basestring)

    groups=groupsplit.split(f)
   
    gdict=Table()
    for g in groups:
        d={}
        lines=g.splitlines()
        for l in lines:
            l=commentstrip(l)
            if l.find('=') > 0:
                p,v=l.split('=',1)
                p=p.strip()
                if not d.has_key(p):
                    d[p]=[]
                d[p].append(v.strip())
        if len(d) > 0:
            assert d.has_key('name')
            assert len(d['name']) == 1
            name=d.pop('name')[0]
            assert not gdict.has_key(name)
            gdict[name]=d
    return gdict

def writeTBL(tbl,filename):
    assert isinstance(tbl,Table)
    f=open(filename,'w')
    f.write(str(tbl))
    f.close()



