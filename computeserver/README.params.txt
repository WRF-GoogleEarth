The params.txt file contains parameters that are specific to each simulation.
Each line must contain exactly one "keyword=value" pair... coments and empty 
lines are not supported.  All parameters can possibly be omitted as defaults
are provided; however, one should always give valid values for the following:

centerLat : decimal number, the latitude of the center of the domain
centerLon : decimal number, the longitude of the center of the domain
ignLat    : decimal number, the latitude of the ignition point
ignLon    : decimal number, the longitude of the ignition point
ignTime   : string, the date and time of the ignition formatted as YYYY-MM-DD_hh:mm:ss
                    (it should be okay to use any standard date/time format standard,
		     but that is largely untested)

Other values that can be specified include:

dx        : decimal number, grid resolution in meters (west-east)
dy        : decimal number, grid resolution in meters (south-north)
nx        : integer, grid size (west-east)
ny        : integer, grid size (south-north)
runTime   : decimal number, time in hours to run the simulation
spinupTime: decimal number, time in hours to spin-up the atmosphere before the ignition time
map_proj  : string, map projection to use (lambert, mercator, polar, rotated_ll, or lat-lon)
ign_radius: decimal number, radius in meters of the ignition
