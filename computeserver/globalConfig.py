#!/usr/bin/env python

import os
from ConfigParser import SafeConfigParser as ConfigParser

thisDir=os.path.dirname(__file__)
if not thisDir:
    thisDir='.'

configFile=[ os.path.join(thisDir,'globalConfig.txt'),
             os.path.expanduser('~/.globalConfig.txt'),
             '/etc/globalConfig.txt',
             os.path.expanduser('/storage/fire/.globalConfig.txt')
           ]
_config=ConfigParser()
_config.read(configFile)

def get(section,option):
    return _config.get(section,option)
