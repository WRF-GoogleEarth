
'''
compile_wrf.py

This is a simple python module to compile WRF from a set of predefined 
configuration options.  It is intended to be used with the accompanying 
module, setup_server.py.  The setup_server.py module is to be used to 
create a tarball containing all possible configuration options for 
the server. 
'''

import subprocess as sp
import sys
import os
import re
import shutil
import tarfile
import hashlib
import git

from setup_server import serverfile,global_desc_file,read_desc,extract_config,ShellCheck,Shell

def git_head_id(path='.'):
    r=git.Repo(path)
    h=r.commits('HEAD')[0]
    return h.id[:8]

def git_diff_from_head(path='.'):
    g=git.Git(path)
    d=g.diff()
    if d != '':
        s=hashlib.sha1(d)
        digest=s.hexdigest()[:8]
    else:
        digest='0'*8
    return (digest,d)
    

def list_compile_options(filename,handle=sys.stdout):
    '''Write to a stream handle all valid configuration options on this server.
       Takes as input the name of the server configuration file produced by setup_server.py.'''
    i=0
    allopts=read_desc(filename)
    for opt in allopts:
        i=i+1
        handle.write('%3i: CC=%-5s FC=%-5s parallel=%-6s debugging=%-3s\n' \
                      % (i,opt['ccomp'],opt['fcomp'],opt['parallel'],      \
                           opt['debug']))
    return allopts

def select_option_from_list(filename,handle=sys.stdout):
    '''Allows the caller to select an option from a list of all valid options on this server.
       Takes as input the name of the server configuration file produced by setup_server.py.'''
    handle.write("Select an option from the following list:\n")
    opts=list_compile_options(filename,handle)
    n=len(opts)
    while True:
        s=sys.stdin.readline().strip('\n')
        i=-1
        try:
            i=int(s)
        except TypeError:
            pass
        if i < 1 or i > n:
            handle.write("Invalid response, choose a number between 1 and %i."%n)
        else:
            extract_config(filename,opts[i-1])
            return

def select_option_0(filename,ccomp,fcomp,parallel,debug,nesting='1'):
    '''Try to select a compilation option by specifying compilers, parallelism, and debugging.
       Inputs:
         filename: name of the server configuration file
         ccomp   : name of the c compiler
         fcomp   : name of the f90 compiler
         parallel: parallel option as a string [serial|smpar|dmpar|dm+sm]
         debug   : debugging option [ON|OFF]
         nesting : only 1 is valid at this time.
      If invalid options are specified, an exception is raised.'''
    opts=read_desc(filename)
    for o in opts:
        if o['ccomp'] == ccomp and o['fcomp'] == fcomp and o['parallel'] == parallel and \
           o['debug'] == debug and o['nesting'] == nesting:
            return o
    else:
        raise Exception("Could not find a compile option matching CC=%s FC=%s parallel=%s debug=%s nesting=%i" \
                         % (ccomp,fcomp,parallel,debug,nesting))

def select_option(filename,ccomp,fcomp,parallel,debug,nesting='1'):
    opt=select_option_0(filename,ccomp,fcomp,parallel,debug,nesting)
    extract_config(filename,opt)

def compile_wrf(target="em_fire"):
    '''Compile WRF.  Assumes that the configuration has already been done (configure.wrf exists).
       Returns a tuple... (val,log)
       val:  True means compilation was successful.
       log:  Long string containing the compilation log.'''
    if target not in ["em_fire","em_real"]:
        raise Exception("Invalid target specified: '%s'."%target)
    if not os.access('configure.wrf',os.R_OK):
        raise Exception("No configuration file found.  You must first run configure manually or use the server interface to extract a preset configuration before compiling.")
    runcompilescript='runcompile.sh'
    f=open(runcompilescript,'w')
    f.write('''#!\bin\bash
#./compile "$@"
''')
    f.close()
    os.chmod(runcompilescript,0755)
    
    cmd=['./compile',target]
    out=ShellCheck(cmd)
    f=open('compile.log','w')
    f.write(out)
    f.close()
    for line in out:
        if re.search('Error',line) is not None:
            break
    else:
        exe=os.path.join('main','wrf.exe')
        if target == 'em_fire':
            real='ideal.exe'
            init=os.path.join('main',real)
        elif target == 'em_real':
            real='real.exe'
            init=os.path.join('main',real)
        if os.access(exe,os.X_OK) and os.access(init,os.X_OK):
            return (True,out)
    sys.stderr.write("Error detected in compile.  Log saved to '%s'."%os.path.abspath(os.path.join('.','compile.log')))
    return (False,out)

def get_commit_path(basedir,commit,diffs='0'*8):
    if isinstance(commit,git.Commit):
        commit=commit.id
    elif isinstance(commit,git.Head):
        commit=commit.commit.id
    elif isinstance(commit,str) and len(commit) >= 8:
        commit=commit[:8]
    else:
        raise Exception("Invalid commit input")
    
    if isinstance(diffs,str) and diffs == '':
        diffs='0'*8
    elif isinstance(diffs,str) and diffs[:4] == 'diff':
        s=hashlib.sha1(diffs)
        diffs=s.hexdigest()[:8]
    elif isinstance(diffs,str) and len(diffs) >= 8:
        diffs=diffs[:8]
    else:
        raise Exception("Invalid diff input")
    if basedir==None:
        return "%s_%s"%(commit,diffs)
    else:
        return os.path.abspath(os.path.join(basedir,"%s_%s"%(commit,diffs)))

def get_bin_path(basedir,opt,commit,diffs='0'*48):
    pth=get_commit_path(basedir,commit,diffs)
    return os.path.join(pth,opt['cfile'])

def compile_all(targetdir,serverfile):
    import cStringIO
    sbuf=cStringIO.StringIO()
    allopts=list_compile_options(serverfile,sbuf)
    desc=sbuf.getvalue().split('\n')

    commitid=git_head_id()
    (d,diff)=git_diff_from_head()
    commitdir=get_commit_path(targetdir,commitid,d)
    if not os.path.isdir(commitdir):
        os.makedirs(commitdir)
    
    if diff != '':
        f=open(os.path.join(commitdir,'diff.log'),'w')
        f.write(diff)
        f.close()

    for i in range(len(allopts)):
        print "Attempting to compile: %s"%desc[i]
        try:
            extract_config(serverfile,allopts[i])
        except Exception , e:
            print "ERROR getting configure option %s"%desc[i]
            print e
            break
        

        simdir=os.path.join(commitdir,allopts[i]['cfile'])

        if not os.path.isdir(simdir) or not os.path.isfile(os.path.join(simdir,'DONE')):
            try:
                shutil.rmtree(simdir)
            except:
                pass
            for target in ['em_fire','em_real']:
                val=False
                try:
                    val,log=compile_wrf(target)
                except Exception , e:
                    print "ERROR compiling option %s %s"%(desc[i],target)
                    print e
                    pass
                if not os.path.isdir(simdir):
                    os.makedirs(simdir)
                if not val:
                    logfile=allopts[i]['cfile']+'_'+target+'.log'
                    f=open(logfile,'w')
                    f.write(log)
                    f.close()
                    print "ERROR in compilation of %s %s"%(desc[i],target)
                    print "log saved to %s"%logfile
                else:
                    print "Success!"
                
                ww=os.path.join(simdir,'wrf.exe')
                if val and target == 'em_fire':
                    os.rename(os.path.join('main','ideal.exe'),os.path.join(simdir,'ideal.exe'))
                    if not os.path.isfile(ww):
                        os.rename(os.path.join('main','wrf.exe'),ww)
                if val and target == 'em_real':
                    os.rename(os.path.join('main','real.exe'),os.path.join(simdir,'real.exe'))
                    if not os.path.isfile(ww):
                        os.rename(os.path.join('main','wrf.exe'),ww)
                llog=os.path.join(simdir,'compile_%s.log'%target)
                if os.path.isfile(llog):
                    os.remove(llog)
                shutil.copy('configure.wrf',llog)
                f=open(llog,'w+')
                f.write('\n'+'*'*80+'\n\n')
                f.write(log)
                f.close()
            f=open(os.path.join(simdir,'DONE'),'w')
            f.write('DONE')
            f.close()

if __name__ == "__main__":
    args=sys.argv[1:];
    if "-t" in args:
        import cStringIO
        sbuf=cStringIO.StringIO()
        allopts=list_compile_options(serverfile,sbuf)
        desc=sbuf.getvalue().split('\n')
        for i in range(len(allopts)):
            print "Attempting to compile: %s"%desc[i]
            try:
                extract_config(serverfile,allopts[i])
            except Exception , e:
                print "ERROR getting configure option %s"%desc[i]
                print e
                break
            try:
                val,log=compile_wrf()
            except Exception , e:
                print "ERROR compiling option %s"%desc[i]
                print e
                break
            if not val:
                f=open(allopts[i]['cfile']+'.log','w')
                f.write(log)
                f.close()
                print "ERROR in compilation of %s"%desc[i]
                print "log saved to %s"%allopts[i]['cfile']+".log"
            else:
                print "Success!"
    elif "-a" in args:
        home=os.path.expanduser('~')
        compile_all(os.path.join(home,'wrfbins'),os.path.join(home,'.wrf_configs.tgz'))
    else:
        select_option_from_list(serverfile)
        compile_wrf("em_fire")
    