import subprocess as sp
#from tee import tee
#from cStringIO import StringIO
import os
import re
import sys
from collections import OrderedDict

from version.sourceControl import GitSourceControl as sc
from cache import resultCache
from log import getLogger

GENERIC_ERROR=-99

_toHex=resultCache.FileCache.toHex
_logger=getLogger().getChild('exe')

def _repr(o):
    if isinstance(o,ExecutionHandler):
        return o._repr()
    else:
        return repr(o)


_c=resultCache.FileCache(repr=_repr)

class ExecutionHandler(sp.Popen):
    '''
    A custom subclass of subproccess.Popen.  In addition to saving its constructor arguments for 
    possible logging, it duplicates the stdout to a file for logging automatically.  It also
    allows subclasses to define custom return codes for binaries that don't properly do so 
    (such as wrf.exe).
    '''
    
    _log='exe.log'
    _expectedFiles={}
    _expectedLog=[]
    _nexpectedFiles={}
    _nexpectedLog=[]
    _paramFiles=[]

    def __init__(self,exe,args=[],shell=True,cwd='.',env={},log=None,stdin=sp.PIPE):
        self._stdin=stdin
        if log is None:
            log = self._log
        _logger.info('creating BaseExecution instance with exe=%s',exe)
        _logger.info('logging to %s',os.path.abspath(self._log))
        self._logname=log
        self._fstdout=open(self._log,'w')
        self._stdout=self._fstdout #tee(self._cstdout,self._fstdout)
        self._stderr=sp.STDOUT
        self._exe=exe
        self._args=args
        self._shell=shell
        self._cwd=cwd
        self._repr=self.__repr__
        extra=''
        executable=None
        
        if isinstance(shell,str):
            executable=shell
            shell=True
            #extra='env ; '
        if shell:
            e=os.environ.copy()
            e.update(env)
        else:
            e=env
        # make env into sorted dict for unique representation
        self._env=OrderedDict([ (k,e[k]) for k in sorted(e) ])
        self._execmd=' '.join([extra,exe]+args)
        _logger.info('final exe=%s',self._execmd)
        #print >> sys.stderr, 'Running: %s' % self._execmd

        if  1 or _c.getCache(self.getFiles,[self],{}) is None:
            sp.Popen.__init__(self,self._execmd,
                              cwd=cwd,env=e,shell=shell,
                              executable=executable,
                              stdin=self._stdin,
                              stdout=self._stdout,
                              stderr=self._stderr)
        else:
            self.poll=lambda : 0
            self.wait=lambda : 0
            self.communicate=lambda : ('','')
            self.checkerror=lambda : 0

    def poll(self):
        sp.Popen.poll(self)
        if self.returncode is None:
            _logger.info('polling process, not finished')
            return None
        self.checkerror()
        return self.returncode

    def wait(self):
        _logger.info('blocking for process')
        sp.Popen.wait(self)
        self.checkerror()
        return self.returncode 

    def communicate(self,input=None):
        _logger.info('communicating with process with input=%s',str(input))
        stdout,stderr=sp.Popen.communicate(self,input)
        self.checkerror()
        return (self._readlog(),'')

    def checkerror(self):
        _logger.info('checking process for error')
        if self.returncode is None:
            raise Exception("checkerror called before process ended")
        elif self.returncode != 0:
            _logger.warn('non-zero return code, %i',self.returncode)
            return self.returncode
        for f,r in self._expectedFiles.iteritems():
            if not self.regexinfile(r,f,dir=self._cwd):
                if r:
                    _logger.warn('file %s did not match pattern %s',f,r.pattern)
                else:
                    _logger.warn('file %s does not exist',f)
                self.returncode = GENERIC_ERROR
                return self.returncode
        for f,r in self._nexpectedFiles.iteritems():
            if self.regexinfile(r,f,dir=self._cwd):
                if r:
                    _logger.warn('file %s matched pattern %s',f,r.pattern)
                else:
                    _logger.warn('file %s exists',f)
                self.returncode = GENERIC_ERROR
                return self.returncode
        for r in self._expectedLog:
            if not self.regexinlog(r):
                _logger.warn('command output did not contain pattern %s',r.pattern)
                self.returncode = GENERIC_ERROR
                return self.returncode
        for r in self._nexpectedLog:
            if self.regexinlog(r):
                _logger.warn('command output contained pattern %s',r.pattern)
                self.returncode = GENERIC_ERROR
                return self.returncode
        return self.returncode

    def _openlog(self):
        if self._fstdout.closed or self._fstdout.mode == 'w':
            self._fstdout=open(self._logname,'r')
    
    def _readlog(self):
        self._openlog()
        return self._fstdout.read()

    def regexinlog(self,r):
        return not (r.search(self._readlog()) is None)

    @classmethod
    def fileindir(cls,filename,dir='.'):
        return os.path.exists(os.path.join(dir,filename))

    @classmethod
    def regexinfile(cls,r,filename,dir='.'):
        if not cls.fileindir(filename,dir):
            return False
        if r is None:
            return True
        s=open(os.path.join(dir,filename),'r').read()
        return not (r.search(s) is None)
    
    @property
    def _hashparams(self):
        params=self._args[:]
        for f in self._paramFiles:
            params.append( open(os.path.join(self._cwd,f),'r').read() )
        p=''.join(params)
        if p:
            return _toHex(p)[:12]
        else:
            return None

    def __repr__(self):
        '''Assemble a unique representation for a class object for caching'''
        r=[ i.strip() for i in (self.__class__,self._version,self._env,os.path.basename(self._execmd),self._hashparams) if isinstance(i,basestring) ]
        return os.path.join(*r)

    @property
    def files(self):
        f=self._expectedFiles.keys()
        return [ os.path.join(self._cwd,i) for i in f ]
    
    #@_c
    def getFiles(self):
        #if self.wait() != 0:
        #    #return tuple()
        self.wait()
        _logger.info('returning files %s',' '.join(self.files))
        return self.files
    
    @property
    def _version(self):
        return sc.version(self._cwd)[:12]

class CompileBaseExecutionHandler(ExecutionHandler):

    _log='compile.log'
    _nexpectedLog=[] #(re.compile('Error'),)
    _compileexe='compile'
    _compileargs=[]

    def __init__(self,cwd='.',env={}):
        exe=os.path.join(cwd,self._compileexe)
        ExecutionHandler.__init__(self,exe,env=env,cwd=cwd,args=self._compileargs,shell='/bin/bash')


class CompileWPSExecutionHandler(CompileBaseExecutionHandler):
    
    _expectedFiles={'geogrid.exe':None,
                   'ungrib.exe':None,
                   'metgrid.exe':None}
    _compileexe='compile'
    _paramFiles=['configure.wps']

class CompileWRFRealExecutionHandler(CompileBaseExecutionHandler):

    _expectedFiles={'main/wrf.exe':None,
                    'main/real.exe':None,
                    'main/ndown.exe':None,
                    'frame/module_internal_header_util.o':None,
                    'frame/module_driver_constants.o':None,
                    'frame/pack_utils.o':None,
                    'frame/module_machine.o':None}
    _compileexe='compile'
    _compileargs=['em_real']
    _paramFiles=['configure.wrf']

class CompileWRFIdealExecutionHandler(CompileBaseExecutionHandler):

    _expectedFiles={'main/wrf.exe':None,
                   'main/ideal.exe':None}
    _compileexe='compile'
    _compileargs=['em_fire']
    _paramFiles=['configure.wrf']

class WPSExecutionHandler(ExecutionHandler):
    
    _nexpectedLog=(re.compile('ERROR'),)
    _wpsexe='.'
    _paramFiles=['namelist.wps']

    def __init__(self,cwd='.',env={},wpspath='.'):
        exe=os.path.join(wpspath,self._wpsexe)
        self._wpspath=wpspath
        ExecutionHandler.__init__(self,exe,env=env,cwd=cwd)

class GeogridExecutionHandler(WPSExecutionHandler):
    
    _log='geogrid_exe.log'
    _wpsexe='geogrid.exe'
    _expectedFiles={'geo_em.d01.nc':None}
    _paramFiles=WPSExecutionHandler._paramFiles[:]
    _paramFiles.extend(['geogrid/GEOGRID.TBL'])


class UngribExecutionHandler(WPSExecutionHandler):

    _log='ungrib_exe.log'
    _wpsexe='ungrib.exe'
    _paramFiles=WPSExecutionHandler._paramFiles[:]
    _paramFiles.extend(['Vtable'])

class MetgridExecutionHandler(WPSExecutionHandler):
    
    _log='metgrid_exe.log'
    _wpsexe='metgrid.exe'
    _paramFiles=WPSExecutionHandler._paramFiles[:]
    _paramFiles.extend(['metgrid/METGRID.TBL'])

class LinkGribExecutionHandler(WPSExecutionHandler):

    _log='link_grib_exe.log'
    _wpsexe='link_grib.csh'
    _expectedFiles={'GRIBFILE.AAA':None}

class WRFBaseExecutionHandler(ExecutionHandler):
     
    _nexpectedLog=(re.compile('FATAL CALLED'),)
    _wrfexe='.'
    _paramFiles=['namelist.input']
    
    def __init__(self,cwd='.',env={},wrfpath='.'):
        exe=os.path.join(wrfpath,self._wrfexe)
        self._wrfpath=wrfpath
        ExecutionHandler.__init__(self,exe,env=env,cwd=cwd)

class RealExecutionHandler(WRFBaseExecutionHandler):

    _log='real_exe.log'
    _wrfexe='real.exe'
    _expectedFiles={'wrfinput_d01':None,'wrfbdy_d01':None}

class IdealExecutionHandler(WRFBaseExecutionHandler):

    _log='ideal_exe.log'
    _wrfexe='ideal.exe'
    _expectedFiles={'wrfinput_d01':None}

class WRFExecutionHandler(WRFBaseExecutionHandler):

    _log='wrf_exe.log'
    _wrfexe='wrf.exe'
    _paramFiles=WRFBaseExecutionHandler._paramFiles[:]
    _paramFiles.extend(['wrfinput_d01'])

def main():
    import shutil,os,glob
    try:
        shutil.rmtree(glob.glob('rm*echo*txt')[0])
    except Exception:
        pass
    class TestExec(ExecutionHandler):
        _expectedFiles={'test.txt':None}
    exe=TestExec('rm test.txt ; echo "test" > test.txt',shell=True)
    exe.wait()
    print exe.getFiles()
    exe=TestExec('rm test.txt ; echo "test" > test.txt',shell=True)
    exe.wait()
    print exe.getFiles()

if __name__ == '__main__':
    main()
