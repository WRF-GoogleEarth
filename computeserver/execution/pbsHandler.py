import subprocess as sp
import os
from time import sleep
from datetime import datetime

from PBSQuery import PBSQuery,PBSError

ppn=12
mpirun='mpirun'
qsub='qsub'
checkinterval=60

class PBSHandler(object):
    
    _script='''#!/bin/bash
#PBS -l nodes=%(nodes)i:ppn=%(ppn)i
#PBS -l walltime=%(walltime)s
#PBS -N %(name)s
#PBS -V
#PBS -j oe
%(queue)s

%(exports)s

cd %(cwd)s
%(mpirun) -np %(np)i -hostfile $PBS_NODEFILE %(exe)s'''

    def __init__(self,exe,walltime='06:00:00',jobname=None,
               args=[],cwd='.',env={},nnodes=1,queue=None):

        self._p=PBSQuery()
        self._run='run.pbs'

        if jobname is None:
            jobname=exe
        
        if queue is not None:
            queue='#PBS -q %s' % queue
        else:
            queue=''

        exports='\n'.join([ 'export %s=%s' % k,v for k,v in env.iteritems()])

        script=_script % {
            'nodes':nnodes,
            'ppn':ppn,
            'walltime':walltime,
            'name':jobname,
            'mpirun':mpirun,
            'cwd':os.path.abspath(cwd),
            'np':nnodes*ppn,
            'exports':exports,
            'exe':exe,
            'queue':queue
        }
        
        run=os.path.join(cwd,self._run)
        open(run,'w').write(script)

        p=sp.Popen([qsub,self._run],shell=True,cwd=cwd,env=env,stdout=sp.PIPE,stderr=sp.STDOUT)
        o=p.communicate()[0]

        self._job=o.strip()

    def wait(self):
        while self.status() != 'Finished':
            sleep(checkinterval)

    def status(self):
        j=self.p.getjobs().get(self._job,None)
        if j is None:
            return 'Finished'

        state=j['job_state']
        if state is 'Q':
            return 'Queued'
        elif state is 'H':
            return 'Held'
        elif state is 'E':
            return 'Exiting'
        elif state is 'R':
            return 'Running'
        elif state is 'F':
            return 'Finished'
        else:
            return 'Unknown'

