
'''
setup_server.py

This python module is intended to be used as an interactive script for a one time setup
of a server.  It will output a tarball which needs to be passed to various functions in
the compile_wrf.py module in order to set up configuration for compilation.  This
script is not bullet-proof... it depends on proper set up of arch/configure_new.defaults
in the wrf package, and it will break on exotic architectures.  However, it should
work well on simple linux machines.

WRF compiles are dependent on a few components, which this script attempts to determine
automatically.  One may need to specify the following:

1.  The path to various compilers f90, c, and parallel variants (i.e. mpif90,mpicc)

2.  The prefix path to the netcdf installation.  The path specified should contain netcdf
    libraries compiled by the specified compiler.  This path should contain 'include' and 'lib'
    directories, which in turn contain 'netcdf.h' and 'libnetcdf.a' respectively.
    
3.  The prefix path the mpirun script for the given compiler.

Note: If the given compiler does not exist on this computer, respond that you do not wish to enable 
      the option.  Also, some options listed will not be valid.  For instance, a gfortran option 
      will have smpar and dm+sm options, but openmp is only supported in new versions of gcc. 
'''

import subprocess as sp
import os
import re
import shutil
import sys
import tarfile

use_cached_opts=True
compilers={}
files={}
serverfile='wrf_configs.tgz'

global_desc_file='configure.desc'
listoptsscript='./getcfgopts.sh'
runcfgscript='./runconfig.sh'
getpathscript='./getpath.sh'

netcdf='/usr'

listoptssh=\
r"""#!/bin/bash

export NETCDF=/usr
rm -f opts_*log baseopts.log
defaultopts="1\n\n"
echo "$defaultopts" | ./configure > baseopts.log
ifssave="$IFS"
IFS=$'\n' 
optlines=( $(cat baseopts.log | sed -n '/^ *[0-9][0-9]*\./p') )
i=0
for line in ${optlines[*]} ; do
  IFS="${ifssave}"
  a=( ${line} )
  description[$i]="${line}"
  optnum[$i]=$(echo ${a[0]} | sed 's/[^0-9]//g')
  parallel[$i]=$(echo ${a[*]} | sed -n 's/.*\(serial\|smpar\|dmpar\|dm+sm\).*/\1/p')
  ((i++))
done

i=0
for o in ${optnum[*]} ; do
  optlog[$i]=$(printf 'opts_%03i.log' $o)
  echo "${o}\n\n" | ./configure > ${optlog[$i]}
  fcomp[$i]=$(cat ${optlog[$i]} | sed -n 's/^SFC\W*=\W*\([^ \W#]*\).*/\1/p' | head -1)
  ccomp[$i]=$(cat ${optlog[$i]} | sed -n 's/^SCC\W*=\W*\([^ \W#]*\).*/\1/p' | head -1)
  mfcomp[$i]=$(cat ${optlog[$i]} | sed -n 's/^DM_FC\W*=\W*\([^ \W#]*\).*/\1/p' |head -1)
  mccomp[$i]=$(cat ${optlog[$i]} | sed -n 's/^DM_CC\W*=\W*\([^ \W#]*\).*/\1/p' |head -1)
  ((i++))
done

for ((i=0 ; i < ${#optnum[*]} ; i++)) ; do
  echo ${optnum[$i]} 1 ${parallel[$i]} ${fcomp[$i]} ${ccomp[$i]} ${mfcomp[$i]} ${mccomp[$i]} ${description[$i]}
done

rm -f opts_*log baseopts.log
exit 0
"""

runcfgsh=\
r"""#!/bin/bash

# $0 $optnum $nestnum $debug $netcdf

rm -f configure.wrf
optnum=$1
nestnum=$2
if [ $# -le 3 ] ; then
  export NETCDF="/usr/local"
else
  export NETCDF="$4"
fi
if [ $3 -eq '--' ] ; then
  debug=' '
else
  debug=$3
fi
echo "$optnum\n$nestnum\n" | ./configure $debug

exit $?
"""

getpathsh=\
r"""#!/bin/bash
which "$1" 2> /dev/null
exit 0
"""

def create_shell_scripts():
    f=open(listoptsscript,'w')
    f.write(listoptssh)
    f.close()
    os.chmod(listoptsscript,0755)
    
    f=open(runcfgscript,'w')
    f.write(runcfgsh)
    f.close()
    os.chmod(runcfgscript,0755)
    
    f=open(getpathscript,'w')
    f.write(getpathsh)
    f.close()
    os.chmod(getpathscript,0755)

def Shell(command):
    c=command
    if not os.access(command[0], os.X_OK):
        raise Exception("Executable %s is not valid." % c[0])
    c=' '.join(command)
    #print c
    p=sp.Popen(c,stdout=sp.PIPE,stderr=sp.STDOUT,shell=True)
    stdout=p.communicate()[0]
    return (p.returncode,stdout)

def ShellCheck(command):
    (status,stdout)=Shell(command)
    if status != 0:
        raise Exception("Command %s returned non-zero status %i."%(command,status))
    return stdout


def getopts():
    stdout=ShellCheck([listoptsscript]).split('\n')
    opts=[]
    for l in stdout:
        c=l.split(' ')
        if c[0] != '':
            o={'num':int(c[0]),'nesting':c[1],'parallel':c[2],'fcomp':c[3],'ccomp':c[4], \
               'mfcomp':c[5],'mccomp':c[6],'description':' '.join(c[7:])}
            opts.append(o.copy())
    return opts

def runconfig(optnum,nestnum,netcdf,debug='--'):
    cmd=[runcfgscript,str(optnum),str(nestnum),debug,netcdf]
    ShellCheck(cmd)
    
def getallopts():
    opts=getopts()
    allopts=[]
    for o in opts:
        for debug in ["OFF","ON"]:
            o['debug']=debug
            allopts.append(o.copy())
    return allopts

def optid(opt):
    return '%s_%s_%s_N%01i_D%s' % \
      (opt['fcomp'],opt['ccomp'],opt['parallel'],int(opt['nesting']),opt['debug'])

def process_response():
    while True:
        s=sys.stdin.readline().strip('\n')
        if s.lower() == "y" or s.lower() == "yes" or s == '':
            return True
        elif s.lower() == "n" or s.lower() == "no":
            return False
        else:
            print "I didn't understand that, please respond with yes or no."
            
def get_path(desc,comp,binary):
    global compilers
    #print desc,comp,binary
    if use_cached_opts and compilers.has_key(comp+binary):
        return compilers[comp+binary]
    p=ShellCheck([getpathscript,binary]).split('\n')[0]
    path=os.path.dirname(p)
    while True:
        print "Enter path to the %s, %s [default: '%s']" % (desc,binary,path)
        s=sys.stdin.readline().strip('\n')
        if s == '' and p != '':
            compilers[comp+binary]=p
            return p
        else:
            if os.access(os.path.join(s,binary),os.F_OK) and s != '':
                compilers[comp+binary]=os.path.join(s,binary)
                return os.path.join(s,binary)
            else:
                print "Could not find '%s' in '%s'!" % (binary,s)
                print 'Do you really want to use this option? [yes|no default: yes]'
                if process_response():
                    return os.path.join(s,binary)
            
def process_opt(opt):
    global files
    ccomp=get_path("C compiler",opt['ccomp'],opt['ccomp'])
    fcomp=get_path("F90 compiler",opt['fcomp'],opt['fcomp'])
    if opt['parallel'] == 'dmpar' or opt['parallel'] == 'dm+sm':
        mccomp=get_path("parallel C compiler for "+opt['ccomp'],opt['ccomp'],opt['mccomp'])
        mfcomp=get_path("parallel F90 compiler for "+opt['fcomp'],opt['fcomp'],opt['mfcomp'])
        mpibin=get_path("mpi binary for this option",opt['fcomp']+opt['ccomp'],'mpirun')+r' -np %i'
    else:
        mccomp=ccomp
        mfcomp=fcomp
        mpibin=''
    netcdf=get_path("NetCDF install directory for this option",opt['fcomp']+opt['ccomp'],'lib/libnetcdf.a')
    netcdf=re.sub(r'/lib/libnetcdf.a','',netcdf)
        
    #print ccomp,fcomp,mccomp,mfcomp
    if opt['debug'] == 'ON':
        d='-d'
    else:
        d='--'
    
    print "Generating configure.wrf for this option."
    runconfig(opt['num'],opt['nesting'],netcdf,debug=d)
    
    g=open('configure.wrf','r+')
    h=open('configure.wrf.new','w')
    line=g.readline()
    while line != '':
        line=re.sub(r'^SCC *= *'+opt['ccomp'],'SCC=%s'%ccomp,line)
        line=re.sub(r'^SFC *= *'+opt['fcomp'],'SFC=%s'%fcomp,line)
        line=re.sub(r'^DM_CC *= *'+opt['mccomp'],'DM_CC=%s'%mccomp,line)
        line=re.sub(r'^DM_FC *= *'+opt['mfcomp'],'DM_FC=%s'%mfcomp,line)
        h.write(line)
        line=g.readline()
    g.close()
    h.close()
    os.remove('configure.wrf')
    os.rename('configure.wrf.new','configure.wrf')
    
    id=optid(opt)
    f='configure_%s.wrf' % id
    os.rename('configure.wrf',f)
    opt['mccomp']=mccomp
    opt['mfcomp']=mfcomp
    opt['mpibin']=mpibin
    opt['netcdf']=netcdf
    files[f]=opt.copy()
    
def open_server_file_desc(filename): # filename == /path/to/serverconfigfile
    '''returns a file handle to the global server descriptor inside the server configuration file passed as an argument'''
    if not tarfile.is_tarfile(filename):
        raise Exception("Could not find server configuration file '%s'"%filename)
    t=tarfile.open(filename,'r')
    try:
        f=t.extractfile(t.getmember(global_desc_file))
    except:
        print "Invalid server configuration file '%s'"%filename
        raise
    return f
    
def configure_desc():
    global files
    f=open(global_desc_file,'w')
    for cfile in files.keys():
        opt=files[cfile]
        f.write("%s,%s,%s,%s,%s,%s,%s,%s\n" % \
                (cfile,opt['ccomp'],opt['fcomp'],opt['nesting'],opt['parallel'],opt['debug'],opt['netcdf'],opt['mpibin']))
    f.close()
    
def read_desc(filename):
    f=open_server_file_desc(filename)
    allopts=[]
    line=f.readline().strip('\n')
    while line != "":
        l=line.split(',')
        if len(l) != 8:
            raise Exception("Invalid server configuration file.")
        opts={}
        opts['cfile']=l[0]
        opts['ccomp']=l[1]
        opts['fcomp']=l[2]
        opts['nesting']=l[3]
        opts['parallel']=l[4]
        opts['debug']=l[5]
        opts['netcdf']=l[6]
        opts['mpibin']=l[7]
        line=f.readline().strip('\n')
        allopts.append(opts.copy())
    return allopts

def extract_config(filename,opt):
    Shell(['./clean','-a'])
    cfile=opt['cfile']
    f=tarfile.open(filename,'r')
    m=f.getmember(cfile)
    f.extractall(members=[m])
    f.close()
    print "Extracting %s"%cfile
    if os.access('configure.wrf',os.W_OK):
        os.remove('configure.wrf')
    os.rename(cfile,'configure.wrf')

if __name__ == '__main__':
    create_shell_scripts()
    print "Getting a list of possible configuration options."
    opts=getallopts()
    for o in opts:
        print '%3i: CC=%-5s FC=%-5s parallel=%-6s debugging=%-3s description="%s"' \
                      % (o['num'],o['ccomp'],o['fcomp'],o['parallel'],             \
                         o['debug'],o['description'])
        print 'Do you want to enable this option? [yes|no default: yes]'
        if process_response():
            process_opt(o)
    configure_desc()
    f=tarfile.open(serverfile,'w:gz')
    f.add(global_desc_file)
    os.remove(global_desc_file)
    for file in files.keys():
        f.add(file)
        os.remove(file)
    f.close()
    print "\nServer config file written to '%s'" % serverfile
    
    
