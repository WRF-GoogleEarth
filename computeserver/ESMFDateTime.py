#!/usr/bin/env python

from dateutil import parser
from datetime import datetime

esmfdatefmt='%Y-%m-%d_%H:%M:%S'

class ESMFDateTime(datetime):
    '''
    Like datetime.datetime but returns string as ESMF format.
    '''
    def __str__(self):
        return self.strftime(esmfdatefmt)

def parse(datestr):
    '''
    Same as dateutil.parser.parse, but can also parse ESMF time strings.
    '''
    datestr=datestr.replace('_',' ')
    d=parser.parse(datestr)
    if d.tzinfo is None:
        tz=0
    else:
        tz=d.tzinfo
    return ESMFDateTime(d.year,d.month,d.day,d.hour,d.minute,d.second,d.microsecond)

