import sys
from glob import glob
import os
import shutil
from time import sleep

from ncEarth import ncWRFFire_mov,uselog,closeFiles

#from wrf2kmz import FireRasterFile,ncKML

sleeptime=30

def sshUpload(file,server,path):
    if server and server != 'localhost':
        import paramiko
        import scpclient
        z=server.split('@')
        if len(z) > 1:
            user=z[0]
            server=z[1]
        else:
            user=None
        client=paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.load_system_host_keys()
        client.connect(server,username=user)
        if not path:
            path='.'
        client.exec_command('mkdir -p %s' % path)
        w=scpclient.Write(client.get_transport(),path)
        w.send_file(file)
        w.close()
        stdin,stdout,stderr=client.exec_command('unzip -o -d %s %s/%s' % (path,path,os.path.basename(file)))
        print 'Extracting %s on remote server' % file
        print >> sys.stdout, stdout
        print >> sys.stderr, stderr
        client.close()
    else:
        try:
            os.makedirs(path)
        except OSError:
            pass
        shutil.copyfile(file,os.path.join(path,os.path.basename(file)))

def watch_directory(dir,varname='fgrnhfx',pattern='wrfout_d01*',output='kmz',upload=None):
    outputfmt='%s/%s_%05i.kmz'
    outputdir='%s' % output
    watchpattern=os.path.join(dir,pattern)
    statusfile=os.path.join(outputdir,'.status_%s'%varname)

    completed=set()

    shutil.rmtree(outputdir,ignore_errors=True)
    os.makedirs(outputdir)
    try:
        os.remove(statusfile)
    except OSError:
        pass
    while True:

        files=set(glob(watchpattern))

        for f in sorted(list(files.difference(completed))):
            
            try:
                index=sorted(list(files)).index(f)
                filename=outputfmt % (outputdir,varname,index)
                
                print 'processing %s' % f
                kmz=ncWRFFire_mov(f)
                kmz.write(varname.upper(),kmz=filename,logscale=uselog(varname))

#                kmz=ncKML()
#                perimeter=FireRasterFile(f).firePerimeterClass()
#                kmz.polygonFromContour(perimeter)
#                kmz.save(filename)

                status=open(statusfile,'a')
                status.write(os.path.basename(filename)+'\n')
                status.close()

                if upload:
                    z=upload.split(':')
                    if len(z) > 1:
                        serv=z[0]
                        pth=''.join(z[1:])
                    else:
                        serv=None
                        pth=upload
                    pth=os.path.join(pth,str(index))
                    sshUpload(filename,serv,pth)
    
                completed.add(f)
            except Exception as e:
                print e
            finally:
                pass
                #closeFiles()


        sleep(sleeptime)
