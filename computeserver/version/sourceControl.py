
import git
import os

class GitSourceControl(object):
    
    @classmethod
    def _open(cls,directory):
        _repo=git.Repo(directory)
        assert _repo.bare == False
        return _repo
   
    @classmethod
    def version(cls,directory,file=None):
        try:
            repo=cls._open(directory)
        except Exception:
            return '0'*40
        if file is None:
            return repo.head.commit.hexsha
        else:
            t=repo.tree()
            return t[file].hexsha
    
    @classmethod
    def checkout(cls,version,directory,file=None):
        repo=cls._open(directory)
        try:
            c=repo.commit(version)
        except git.BadObject:
            c=repo.commit('origin/'+version)
        if file is None:
            repo.head.reference=c
            repo.head.reset(index=True,working_tree=True)
        else:
            raise Exception("Checking out a subrepository not implemented for this class")

    @classmethod
    def clone(cls,source,target,version='master'):
        repo=cls._open(source)
        repo.clone(os.path.abspath(target))
        repo=cls._open(target)
        cls.checkout(version,target)

class SourceControl(GitSourceControl):
    pass
