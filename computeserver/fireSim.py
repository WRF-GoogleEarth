#!/usr/bin/env python

import os
import sys
import shutil
from datetime import timedelta,datetime
from glob import glob
import threading
import tarfile

from version import sourceControl
from globalConfig import get
from execution.ExecutionHandler import CompileWPSExecutionHandler,CompileWRFRealExecutionHandler, \
                                       GeogridExecutionHandler,UngribExecutionHandler,            \
                                       MetgridExecutionHandler,LinkGribExecutionHandler,          \
                                       RealExecutionHandler,WRFExecutionHandler
from fireParams import readParams
from ESMFDateTime import parse
from WPS import downloadGRIB,GeogridData
from NML import wrffire_namelist
from log import getLogger,setupLogging
from visualize import watch_directory

_logdir=get('server','logdir') 

try:
    os.makedirs(_logdir)
except Exception:
    pass

_uniquename=datetime.now().isoformat()
_logfile=os.path.join(_logdir,_uniquename)
setupLogging(_logfile)

_metersperdegree=111000.
_templatewpsnml='WPS/namelist.wps'
_templatewrfnml='wrfv2_fire/test/em_real/namelist.input'
_logger=getLogger()


def initializeDir(runDir,version='master'):
    if os.path.exists(runDir):
        shutil.rmtree(runDir)
    os.makedirs(runDir)
    sourceControl.GitSourceControl.clone(get('server','repoDir'),runDir,version)
    shutil.copy(get('server','wpsConfig'),os.path.join(runDir,'WPS'))
    shutil.copy(get('server','wrfConfig'),os.path.join(runDir,'wrfv2_fire'))
    env={'NETCDF':get('server','netCDFpath')}
    wrfcompile=CompileWRFRealExecutionHandler(cwd=os.path.join(runDir,'wrfv2_fire'),env=env)
    r=wrfcompile.wait()
    wrfcompile.getFiles()
    assert r == 0
    wpscompile=CompileWPSExecutionHandler(cwd=os.path.join(runDir,'WPS'),env=env)
    r=wpscompile.wait()
    wpscompile.getFiles()
    assert r == 0
    try:
        shutil.copy(os.path.join(runDir,_templatewpsnml),os.path.join(runDir,'WPS'))
    except Exception:
        pass
    try:
        shutil.copy(os.path.join(runDir,_templatewrfnml),os.path.join(runDir,'wrfv2_fire','run'))
    except Exception:
        pass
    try:
        os.remove(os.path.join(wrfdir,'run','namelist.fire'))
    except OSError:
        pass
    shutil.copy(os.path.join(wrfdir,'test','em_real','namelist.fire'),os.path.join(wrfdir,'run','namelist.fire'))


def initializeDirTemplate(runDir):
    if os.path.exists(runDir):
        shutil.rmtree(runDir)
    os.makedirs(runDir)
    templatedir=get('server','templateDir')
    t=tarfile.TarFile(templatedir,'r')
    t.extractall(runDir)

def getProjFromIGN(params):
    p=params
    map_proj=p['map_proj']
    lat=float(p['centerLat'])
    lon=float(p['centerLon'])
    dx=float(p['dx'])
    dy=float(p['dy'])
    nx=int(p['nx'])
    ny=int(p['ny'])
    x=dx*nx/_metersperdegree
    y=dy*ny/_metersperdegree
    ref_lat=lat-y/2.
    ref_lon=lon-x/2.
    proj=wrffire_namelist.ProjInfo(map_proj,lat,lon,lat,lat,lon)
    return proj

def setupSim(runDir,paramFile):
    wpsdir=os.path.join(runDir,'WPS')
    wrfdir=os.path.join(runDir,'wrfv2_fire')
    wpsnml=os.path.join(wpsdir,'namelist.wps')
    wrfnml=os.path.join(wrfdir,'run','namelist.input')
    

    p=readParams(paramFile)
    igntime=parse(p['ignTime'])
    spinup=timedelta(hours=float(p['spinupTime']))
    runtime=timedelta(hours=float(p['runTime']))

    dgrib=downloadGRIB.gribSources['NARR']
    starttime=dgrib.getNearestBefore(igntime-spinup)
    endtime=dgrib.getNearestAfter(igntime+runtime)
    gribfiles=dgrib.getFileRange(starttime,endtime)
    
    dgrib.linkGrib(wpsdir,gribfiles)
    
    bounds=GeogridData.getBounds(float(p['centerLon']),
                                 float(p['centerLat']),
                                 float(p['dx']),float(p['dy']),
                                 int(p['nx']),int(p['ny']))
    geogridTBL=os.path.join(wpsdir,'geogrid','GEOGRID.TBL')
    nedfile=GeogridData.getData('ned3',bounds)
    nedfile[0]=os.path.abspath(nedfile[0])
    GeogridData.fixTable(geogridTBL,os.path.dirname(nedfile[0]),'ned3')
    fuelfile=GeogridData.getData('anderson13',bounds)
    fuelfile[0]=os.path.abspath(fuelfile[0])
    GeogridData.fixTable(geogridTBL,os.path.dirname(fuelfile[0]),'anderson13')
    
    proj=getProjFromIGN(p)
    dargs={
        'proj':proj,
        'nx':int(p['nx']),
        'ny':int(p['ny']),
        'dx':float(p['dx']),
        'dy':float(p['dy']),
        'start_date':starttime,
        'end_date':endtime,
        'interval_seconds':dgrib.interval_seconds()
    }
    domain=wrffire_namelist.Domain(**dargs)

    ign=wrffire_namelist.FireIgnition()
    ign.add(start=(float(p['ignLon']),float(p['ignLat'])),
            end=(float(p['ignLon']),float(p['ignLat'])), \
            radius=float(p['ign_radius']),time=(igntime-starttime).total_seconds())

    domain.SetFire(ign)
    nml=wrffire_namelist.WPSnml(wpsnml)
    nml.SetDomain(domain)
    nml.setGeogPath(get('server','wpsData'))
    f=open(wpsnml,'w')
    f.write(str(nml))
    f.close()

    nml=wrffire_namelist.WRFnml(wrfnml)
    nml.SetDomain(domain)
    f=open(wrfnml,'w')
    f.write(str(nml))
    f.close()

def runSim(runDir,uploadPath=None):
    wpsrundir=os.path.join(runDir,'WPS')
    wrfrundir=os.path.join(runDir,'wrfv2_fire','run')
    env={'LD_LIBRARY_PATH':get('server','ldPath')}
    print env
    e=GeogridExecutionHandler(cwd=wpsrundir,env=env)
    e.getFiles()
    e=UngribExecutionHandler(cwd=wpsrundir,env=env)
    e.getFiles()
    e=MetgridExecutionHandler(cwd=wpsrundir,env=env)
    e.getFiles()
    metfiles=e.getFiles()

    for f in glob(os.path.join(wpsrundir,'met_em*')):
        try:
            os.remove(os.path.join(wrfrundir,os.path.basename(f)))
        except OSError:
            pass
        os.symlink(os.path.abspath(f),os.path.join(wrfrundir,os.path.basename(f)))

    e=RealExecutionHandler(cwd=wrfrundir,env=env)
    e.getFiles()
    e=WRFExecutionHandler(cwd=wrfrundir,env=env)
    
    outputdir=os.path.join(runDir,'kmz')
    t=threading.Thread(target=watch_directory,args=(wrfrundir,),kwargs={'output':outputdir,'upload':uploadPath})
    t.setDaemon(True)
    t.start()

    e.wait()

def run(session,paramfile):
    print 'starting session: %s' % session
    print 'using params:'
    print open(paramfile,'r').read()
    runDir=os.path.join(get('server','mainDir'),str(session))
    print 'runDir=%s' % runDir

    initializeDirTemplate(runDir)
    f=open(paramfile,'r')
    p=readParams(f)

    uploadPath=p['uploadpath']

    os.chdir(runDir)

    setupSim(runDir,f)
    runSim(runDir,uploadPath=uploadPath) 

if __name__ == '__main__':
    import sys
    from WPS.SeamlessServer import setHTTPDebug
    setHTTPDebug()
    run(sys.argv[1],sys.argv[2])
