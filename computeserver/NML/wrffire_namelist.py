'''
Created on Oct 29, 2009

@author: jbeezley
'''
import re
from nmlparse import Namelist
from NMLDict import *
import ESMFDateTime

def assertintpos(param,val):
    if int(val) != float(val) or int(val) <= 0:
        raise TypeError

class NMLException(Exception):
    def __str__(self):
        return "Invalid namelist."
    
class NMLParamError(NMLException):
    def __init__(self,param):
        NMLException.__init__(self)
        self.param=param
    def __str__(self):
        return NMLException.__str__(self)+" "+ \
               "Parameter Error in "+param

class NMLTypeError(NMLParamError):
    def __init__(self,param,expected,got):
        NMLParamError.__init__(self,param)
        self.expected=expected
        self.got=got
    def __str__(self):
        return NMLParamError.__str__(self) + " " + \
               "Expected type %s got " %(self.expected,self.got)
        
class NMLValueError(NMLParamError):  
    def __init__(self,param,value):
        NMLParamError.__init__(self,param)
        self.value=value
    def __str__(self):
        return NMLParamError.__str__(self)+" "+\
               "Invalid value %s"%str(value)
               
class NMLOpError(NMLException):
    def __init__(self,param1,val1,op,param2,val2):
        NMLException.__init__(self)
        self.param1=param1
        self.param2=param2
        self.val1=val1
        self.val2=val2
        self.op=op
    def __str__(self):
        return NMLException.__str__(self)+" "+\
               "%s=%s must be %s %s=%s" % (self.param1,self.val1, \
                                           self.op,               \
                                           self.param2,self.val2)
           
def AssertOp(d,param1,op,param2,i):
    if not "%s %s %s" %(str(d[param1][i]),op,str(d[param2][i])):
        raise NMLOpError(param1,d[param1][i],op,param2,d[param2][i])
    
def AssertVal(d,param,op,val,i):
    if not "%s %s %s" %(str(d[param][i]),op,str(val)):
        raise NMLOpError(param,d[param][i],op,'value',str(val))

class WPSnml(Namelist):
    def numDomains(self):
        return int(self['share']['max_dom'][0])

    def SetMaxDom(self,maxdom):
        self['share']['max_dom']=[maxdom]

    def SetDomain(self,domain,parent=0,count=0):
        domain.check()
        if parent == 0:
            self.SetMaxDom(domain.num())

        myid=count+1
        d=self['geogrid']
        d['e_we'][count]=domain['nx']
        d['e_sn'][count]=domain['ny']
        if parent == 0:
            d['dx'][0]=domain['dx']
            d['dy'][0]=domain['dy']
        d['i_parent_start'][count]=domain['istart']
        d['j_parent_start'][count]=domain['jstart']
        d['parent_id'][count]=parent
        d['parent_grid_ratio'][count]=domain['grid_ratio']
        self['share']['subgrid_ratio_x'][count]=domain['sr_x']
        self['share']['subgrid_ratio_y'][count]=domain['sr_y']
        self['share']['start_date'][count]=str(domain['start_date'])
        self['share']['end_date'][count]=str(domain['end_date'])

        for c in domain.children:
            count=count+1
            count=self.SetDomain(c,myid,count)

        if parent == 0:
            assert domain['proj'] is not None
            self['geogrid'].update(domain['proj'])

        return count

    def setGeogPath(self,pth):
        self['geogrid']['geog_data_path']=[pth]

    def getGeogPath(self):
        return self['geogrid']['geog_data_path']

class WRFnml(Namelist):
    def SetRunTime(self,timeinsec):
        self['time_control']['run_days'][0]=0
        self['time_control']['run_hours'][0]=0
        self['time_control']['run_minutes'][0]=0
        self['time_control']['run_seconds'][0]=timeinsec

    def __setitem__(self,key,value):
        raise Exception("Cannot add keys to a namelist class.")
    
    def check(self):
        val=True
        try:
            for k in self.keys():
                group=self[k]
                checkGroup(k)
        except:
            val=False
        return val

    def numDomains(self):
        return int(self['domains']['max_dom'][0])

    def checkGroup(self,group):
        for k in group.keys():
            param=k
            vals=group[param]
            if not isinstance(vals,list):
                raise NMLTypeError(param,"list",vals.__class__)
            if not len(vals) >= self.numDomains() or not len(vals) == 1:
                raise NMLValueError(param)
    
    def SetDomain(self,domain,parent=0,count=0):
        domain.check()
        if parent == 0:
            self.SetMaxDom(domain.num())
            
        myid=count+1
        d=self['domains']
        d['e_we'][count]=domain['nx']
        d['e_sn'][count]=domain['ny']
        d['e_vert'][count]=domain['nz']
        d['dx'][count]=domain['dx']
        d['dy'][count]=domain['dy']
        #d['ztop'][count]=domain['ztop']
        d['i_parent_start'][count]=domain['istart']
        d['j_parent_start'][count]=domain['jstart']
        d['grid_id'][count]=myid
        d['parent_id'][count]=parent
        d['parent_grid_ratio'][count]=domain['grid_ratio']
        d['parent_time_step_ratio'][count]=domain['time_ratio']
        d['sr_x'][count]=domain['sr_x']
        d['sr_y'][count]=domain['sr_y']

        if domain.history:
            self['time_control']['history_interval_s'][count]=domain.history
            self['time_control']['history_interval_m'][count]=0
            self['time_control']['history_interval_h'][count]=0
            self['time_control']['history_interval_d'][count]=0
            self['time_control']['history_interval'][count]=0

        t=self['time_control']
        start=domain['start_date']
        end=domain['end_date']
        t['start_year'][count]=start.year
        t['start_month'][count]=start.month
        t['start_day'][count]=start.day
        t['start_hour'][count]=start.hour
        t['start_minute'][count]=start.minute
        t['start_second'][count]=start.second

        t['end_year'][count]=end.year
        t['end_month'][count]=end.month
        t['end_day'][count]=end.day
        t['end_hour'][count]=end.hour
        t['end_minute'][count]=end.minute
        t['end_second'][count]=end.second

        t['frames_per_outfile'][count]=domain['frames_per_outfile']
            
        self.SetFire(domain.fires,domainid=myid)
               
        for c in domain.children:
            count=count+1
            count=self.SetDomain(c,myid,count)

        if parent == 0:
            d['time_step']=[domain['time_step']]
            d['time_step_fract_num']=[domain['time_step_num']]
            d['time_step_fract_den']=[domain['time_step_den']]
            self.SetRestartInterval(int(domain['restart_interval']))
            
        return count
    
    def SetMaxDom(self,maxdom):
        c=self.numDomains()
        for gname in self.keys():
            g=self[gname]
            for param in g:
                vals=g[param]
                if not (gname,param) in globset:
                    vals=vals[:c]
                    for i in range(maxdom-c):
                        vals.append(vals[c-1])
                    g[param]=vals
        self['domains']['max_dom']=[maxdom]
        
    def MakeDict(self):
        domset=set([])
        globset=set([])
        for gname in self.keys():
            g=self[gname]
            for param in g:
                vals=g[param]
                if len(vals) == 1 and not gname=='fire':
                    globset.add((gname,param))
                else:
                    domset.add((gname,param))
        f=open('NMLDict.py','w')
        f.write('domset=%s\n'%str(domset))
        f.write('globset=%s\n'%str(globset))
        
    def SetFire(self,fires,domainid=None,normalized=False):
        Figns=fires
        if domainid is None:
            domainid=self.numDomains()
        #print domainid,(fires is None)
        i=domainid-1
        fire=self['fire']
        if fires is None or len(fires) == 0:
            fire['ifire'][i]=0
            return
        fire['ifire'][i]=2
        fire['fire_num_ignitions'][i]=len(fires)
#        if normalized:
#            scl=(float(self['domains']['dx'][i])*int(self['domains']['e_we'][i]),\
#                 float(self['domains']['dy'][i])*int(self['domains']['e_sn'][i]))
        for j in range(1,1+len(Figns)):
            ign=Figns[j-1]
#            ign['fire_ignition_start_x']=ign['fire_ignition_start_x']*scl[0]
#            ign['fire_ignition_start_y']=ign['fire_ignition_start_y']*scl[1]
#            ign['fire_ignition_end_x']=ign['fire_ignition_end_x']*scl[0]
#            ign['fire_ignition_end_y']=ign['fire_ignition_end_y']*scl[1]
#            ign['fire_ignition_radius']=ign['fire_ignition_radius']*(scl[0]+scl[1])/2.
            for k in ign.keys():
                key="%s%i"%(k,j)
                if not ign.has_key(key):
                    #print self['domains']
                    fire[key]=[0.]*self.numDomains()
                fire[key][i]=ign[k]
                
    def SetDebug(self,val=True):
        if val:
            self['time_control']['debug_level'][0]=101
            self['fire']['fire_print_file']=[0 for i in range(self.numDomains())]
            self['fire']['fire_print_msg']=[0 for i in range(self.numDomains())]
        else:
            self['time_control']['debug_level'][0]=0
            #self['fire']['fire_print_file']=[0 for i in range(self.numDomains)]
            self['fire']['fire_print_msg']=[1 for i in range(self.numDomains())]
            
    def SetRestartInterval(self,i):
        self['time_control']['restart_interval'][0]=i

class ProjInfo(dict):
    def __init__(self,map_proj,ref_lat,ref_lon,truelat1,truelat2,stand_lon):
        dict.__init__(self)
        self['map_proj']=[map_proj]
        self['ref_lat']=[float(ref_lat)]
        self['ref_lon']=[float(ref_lon)]
        self['truelat1']=[float(truelat1)]
        self['truelat2']=[float(truelat2)]
        self['stand_lon']=[float(stand_lon)]

class Domain(dict):
    default={'time_step':0,'time_step_num':25,'time_step_den':100, \
             'nx':42,'ny':42,'nz':41,'dx':60.,'dy':60.,#'ztop':1500., \
             'istart':0,'jstart':0,'grid_ratio':1,'time_ratio':1, \
             'sr_x':10,'sr_y':10,'start_date':'0001-01-01_01:01:00',\
             'end_date':'0001-01-02_01:01:00','proj':None,    \
             'geog_data_res':'30s','interval_seconds':21600,  \
             'frames_per_outfile':1,'restart_interval':9999}
    def __init__(self,**args):
        dict.__init__(self)
        self.update(self.default)
        self.update(args)
        self.children=[]
        self.parent=None
        self.check()
        self.fires=None
        self.history=None

        if isinstance(self['start_date'],basestring):
            self['start_date']=ESMFDateTime.parse(self['start_date'])
        if isinstance(self['end_date'],basestring):
            self['end_date']=ESMFDateTime.parse(self['end_date'])
        
    def check(self):
        for k in ['nx','ny','nz','dx','dy','istart','jstart','sr_x','sr_y',\
                  'grid_ratio','time_ratio','time_step','time_step_num','time_step_den']:
            if not self.has_key(k):
                raise Exception("Invalid Domain: needs key %s" % k)
            
    def create_child(self,**args):
        child=Domain(**args)
        self.children.append(child)
        if child['grid_ratio'] not in [2,3,4,5]:
            raise Exception("grid ratio %s not valid."%str(self['grid_ratio']))
        child['dx']=self['dx']/child['grid_ratio']
        child['dy']=self['dy']/child['grid_ratio']
        if child['istart'] not in range(1,self['nx']) or \
           child['jstart'] not in range(1,self['ny']) or \
           child['nx'] % child['grid_ratio'] != 0  or    \
           child['ny'] % child['grid_ratio'] != 0  or    \
           child['istart']+child['nx']/child['grid_ratio'] > self['nx'] or \
           child['jstart']+child['ny']/child['grid_ratio'] > self['ny']:
            raise Exception("Invalid child grid: %s"%str(child))
        child.parent=self
        return child
    
    def num(self):
        count=1
        for c in self.children:
            count=count+c.num()
        return count
    
    def SetFire(self,ign):
        self.fires=ign
        
    def SetHistoryInterval(self,i):
        if i is not None:
            self.history=i
        
    def SetAll(self,history=None):
        if history:
            self.SetHistoryInterval(history)
        for c in self.children:
            c.SetAll(history=history)
        
class FireIgnition(list):
    def __init__(self):
        list.__init__(self)
        self.maxignitions=3
        self.ifire=2
        self.nofire=0
        #self.add(**args)
        
    def add(self,**args):
        if(len(self) > self.maxignitions):
            raise Exception("WRF-Fire handles only %i ignitions." % self.maxignitions)
        self.append( {                               \
           'fire_ignition_start_lon':args['start'][0], \
           'fire_ignition_start_lat':args['start'][1], \
           'fire_ignition_end_lon':args['end'][0],     \
           'fire_ignition_end_lat':args['end'][1],     \
           'fire_ignition_radius':args['radius'],    \
           'fire_ignition_start_time':args['time'],  \
           'fire_ignition_end_time':args['time'],    \
           'fire_ignition_ros':1
           })

if __name__ == '__main__':
    f=WRFnml('/Users/jbeezley/namelist.input')
    d=Domain(nx=21,ny=21)
    d0=d.create_child(nx=21,ny=21,grid_ratio=3,time_ratio=3,istart=2,jstart=2)
    d1=d0.create_child(nx=21,ny=21,grid_ratio=3,time_ratio=3,istart=2,jstart=2)
    d5=d0.create_child(nx=21,ny=21,grid_ratio=3,time_ratio=3,istart=10,jstart=10)
    d2=d1.create_child(nx=21,ny=21,grid_ratio=3,time_ratio=3,istart=2,jstart=2)
    fire=FireIgnition()
    fire.add(start=(.5,.500),end=(.600,.600),radius=.05,time=2)
    fire.add(start=(.700,.1500),end=(.800,.1600),radius=.02,time=2.5)
    d5.SetFire(fire)
    d.SetAll(history=10)
    f.SetDebug(False)
    f.SetDomain(d)
    f.SetRunTime(300)
    f.SetRestartInterval(10)
    #f.MakeDict()
    print str(f)

