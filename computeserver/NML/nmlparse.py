#!/usr/bin/env python

'''
A simple python only parser module for fortran namelists.  This module
probably cannot correctly parse all possible namelists, but it should
work with most common conventions.  It will strip all 
'''

import re
import sys
import os
import cStringIO

# compile a bunch of regex's used to parse namelists
_comments=re.compile(r'[!#].*')                # comments begining with # or !
_string=re.compile(r'("[^"]*"'+r"|'[^']*')")   # strings ".*" or '.*'
_isstring=re.compile(r'^[\'"]')                # for quick checking if a result is a string
_white=r'[\s\t]'                               # white space characters
_black=r'\b[a-zA-Z][a-zA-Z0-9_]*\b'            # valid fortran variables
_rwhite=re.compile(_white)                     # white space regex
_istrue=re.compile(r'(\.true\.|t)',re.I)       # fortran logical true
_isfalse=re.compile(r'(\.false\.|f)',re.I)     # fortran logical false

# several numeric regex strings:
_snumeric=r'[+-]?\d*\.?\d*[eEdD]?[-+]?\d*'     # a symplified numeric regex... will match all floats
                                               # as well as some non-floats (i.e. ".") needed for
                                               # parsing complex types (x,y)... 

_float=r'[+-]?\d+\.?\d*[eEdD]?[-+]?\d*|[+-]?\d*\.?\d*[eEdD]?[-+]?\d+' # float regex string
_int=r'[-+]\d+'                                # integers
_real=_float                                   # synonym for float
_complex=r'\(' + _real + r',' + _real + r'\)'  # complex type regex string
_scomplex=r'(\(' + _snumeric + r',' + _snumeric + r'\))' # symplified complex regex string (using _snumeric)
_rscomplex=re.compile(_scomplex)
_numeric='(' + _real + '|' + _complex + ')'    # matches any fortran numeric

# compiled regex's of the previous strings
_rfloat=re.compile(_float)
_rint=re.compile(_int)
_rreal=_rfloat
_rcomplex=re.compile(_scomplex)
_rnumeric=re.compile(_numeric)

# unique representation of fortran logicals
_true='.true.'
_false='.false.'

_group=re.compile(r'&'+_black)              # start of a namelist group
_endgroup=re.compile(r'/')                  # end of a namelist group
_continue=re.compile(r'&'+_white+r'*$|\\$') # continuation characters sometimes used at the end of the line
_param=re.compile(_black+r'=')              # parameter name
_iscomplex=re.compile(r'^\(')               # for quick checking if a result is complex
_multval=re.compile(r'^(\d+)(\*)(.*)$')     # for fortran namelist output support... 
                                            # often repeated namelist values will be combined rather than
                                            # printed separately, i.e. 
                                            # 3*1.0 rather than 1.0,1.0,1.0

# If set to true than only strings that are quoted are parsed as strings.  All unquoted
# values in the namelist must be parseable as a numeric type.  Setting this to True
# will improve error detection, but some namelists will not be readable, such as
# those written out by a fortran program.
onlyQuotedStrings=False

class NMLParseError(Exception):
    '''
    A unified Exception class for all parsing errors.
    '''
    pass

def convBool(s):
    if _istrue.match(s):
        return True
    elif _isfalse.match(s):
        return False
    else:
        raise NMLParseError("Invalid bool: %s"%s)

def convString(s):
    if _isstring.match(s):
        return s[1:-1]
    else:
        raise NMLParseError("Invalid string: %s"%s)

def convInt(s):
    try:
        return int(s)
    except:
        raise NMLParseError("Invalid int: %s" %s)

def convReal(s):
    try:
        s=s.lower().replace('d','e')
        return float(s)
    except:
        raise NMLParseError("Invalid float: %s" %s)

def convComplex(s):
    try:
        s=s[1:-1].split(',')
        if len(s) != 2:
            raise Exception
        return convReal(s[0]) + convReal(s[1])*1j
    except:
        raise NMLParseError("Invalid complex: %s" %s)

def convXstring(s):
    return s

convtype={'bool':convBool,'string':convString,'int':convInt,'real':convReal,'complex':convComplex,'xstring':convXstring}

def remove(s,r):
    return r.sub('',s)

def removecomments(s):
    return remove(s,_comments)

def removewhite(s):
    return remove(s,_rwhite)

def removecontinuation(s):
    return remove(s,_continue)

def removeall(s):
    return removewhite(removecontinuation(removecomments(s)))

def cleanline(line):
    llist=_string.split(line)
    hascomment=False
    for i in xrange(len(llist)):
        l=llist[i]
        if hascomment:
            llist[i]=''
        elif not _isstring.match(l):
            hascomment=_comments.search(l) != None
            llist[i]=removeall(l)
    return ''.join(llist)

def getgroups(nml):
    lines=nml.split('\n')
    grouplist={}
    ingroup=''
    for line in lines:
        llist=_string.split(cleanline(line))
        for l in llist:
            isstring=_isstring.match(l)
            if ingroup and isstring:
                grouplist[ingroup].append(l)
            elif not ingroup and isstring:
                pass
            elif ingroup and not isstring:
                if _endgroup.search(l):
                    ingroup=''
                else:
                    grouplist[ingroup].append(l)
            elif not ingroup and not isstring:
                grp=_group.match(l)  # only matching '&group' at the beginning of the line
                if grp:
                    ingroup=grp.group()[1:]
                    grouplist[ingroup]=[_group.sub('',l)]  # otherwise need to fix here
    return grouplist
                
def getparams(llist):
    paramlist={}
    inparam=''
    for l in llist:
        isstring=_isstring.match(l)
        pmatch=_param.match(l)
        if not isstring and pmatch:
            inparam=pmatch.group()[:-1]
            l=_param.sub('',l)
            paramlist[inparam]=[l]
        elif inparam and l:
            paramlist[inparam].append(l)
        elif not inparam and l:
            raise NMLParseError("Could not parse content as a parameter: %s" % l)
    return paramlist

def multval(val):
    m=_multval.match(val)
    n=1
    if m:
        n,t,s=m.groups()
        val=s
        n=int(n)
    return val,n

def getvalues(llist):
    values=[[]]
    nn=[0]
    for ll in llist:
        for l in _rscomplex.split(ll):
            lone,n=multval(l)
            isstring=_isstring.match(lone)
            iscomplex=_rcomplex.match(lone)
            if isstring or iscomplex:
                values[-1].append(lone)
                nn[len(values)-1]=n
            else:
                for v in l.split(','):
                    v,n=multval(v)
                    values[-1].append(v)
                    nn[len(values)-1]=n
                    values.append([])
                    nn.append(0)

    values=[ ''.join(v) for v in values ]
    vv=[]
    assert len(nn) == len(values)
    for i in xrange(len(nn)):
        vv.extend([values[i]]*nn[i])
    return [ v for v in vv if len(v) > 0 ]

def matchall(v,r):
    return r.sub('',v) == ''

def getType(value):
    if matchall(value,_string):
        return 'string'
    elif matchall(value,_rint):
        return 'int'
    elif matchall(value,_rreal):
        return 'real'
    elif matchall(value,_rcomplex):
        return 'complex'
    elif matchall(value,_istrue) or matchall(value,_isfalse):
        return 'bool'
    else:
        #raise NMLParseError('Could not parse value string %s' % value)
        return 'xstring'

def convertType(values):
    type=''
    for v in values:
        t=getType(v)
        if not type:
            type=t
        else:
            if (type[-6:] == 'string') != (t[-6:] == 'string'):
                raise NMLParseError('Value list contains both numeric and string types: %s' % ' '.join(values))
            elif (type == 'int'):
                type=t
            elif (type == 'real') and (t != 'int'):
                type=t
    return [ convtype[type](v) for v in values ]

def tostring(v):
    if isinstance(v,bool):
        if v:
            return _true
        else:
            return _false
    elif isinstance(v,float) and v.is_integer():
        return str(int(v))
    elif isinstance(v,str):
        v=v.replace('"',"'")
        return '"'+v+'"'
    else:
        return str(v)

def writenml(nml,f=None):
    if f is None:
        g=cStringIO.StringIO()
    else:
        g=f
    for group in nml:
        grouplist=nml[group]
        g.write('&'+group+'\n')
        for param in sorted(grouplist):
            values=grouplist[param]
            g.write(param+'=')
            try:
                values=[tostring(s) for s in values]
            except TypeError:
                values=[tostring(values)]
            g.write(','.join(values) + '\n')
        g.write('/\n')
    g.flush()
    if f is None:
        return g.getvalue()

def readnamelist(nml):
    groups=getgroups(nml)
    for g in groups:
        params=getparams(groups[g])
        for p in params:
            values=getvalues(params[p])
            params[p]=convertType(values)
        groups[g]=params
    return groups

class Namelist(dict):
    def __init__(self,file):
        dict.__init__(self)
        self.update(self.parse(file))

    @classmethod
    def _open(cls,sfile):
        data=sfile
        if isinstance(sfile,str):
            if os.path.exists(sfile):
                sfile=open(sfile,'r')
                data=sfile.read()
        else:
            data=sfile.read()
        return data

    @classmethod
    def parse(cls,file):
        f=cls._open(file)
        return readnamelist(f)

    def __str__(self):
        return writenml(self)

    def groups(self):
        return self.keys()

    def params(self,group):
        return self[group].keys()


def tests():
    '''Run several self tests'''
    from cStringIO import StringIO 
    nmls=[]
    nmld=[]

    nmls.append('''

    ''')
    nmld.append({})
    for i in xrange(len(nmls)):
        cnml=readnamelist(nmls[i])
        assert cnml == nmls[i]
        f=StringIO()
        writenml(nmls[i],f)
        dnml=f.getvalue()
        assert cnml == readnamelist(dnml)

if __name__ == '__main__':
    if len(sys.argv[1:]) > 0:
        for files in sys.argv[1:]:
            f=open(sys.argv[1],'r')
            nml=f.read()
            writenml(readnamelist(nml))
    else:
        tests()

