#!/bin/bash

# Looks for the OS System
DISTRIB_ID=$(grep DISTRIB_ID /etc/*-release)
# Takes the left side of the file variable out
DISTRIB_ID=${DISTRIB_ID#*=}

if [[ $DISTRIB_ID != "Ubuntu" ]]; then
    echo "We don't support your OS System, please follow 'setup_universal.txt' to know what you need to install."
    exit 2
fi

# Installs Dependency Files
sudo apt-get update
sudo apt-get install gcc
sudo apt-get install g++
sudo apt-get install python
sudo apt-get install python-setuptools
sudo apt-get install python-dev
sudo apt-get install apache2
sudo apt-get install libapache2-mod-python
sudo apt-get install mysql-server
sudo apt-get install python-mysqldb
sudo apt-get install mail
sudo apt-get install mailman
sudo apt-get install sendmail
sudo apt-get install postfix

cd /tmp/
django=Django-1.4.tar.gz

wget https://www.djangoproject.com/download/1.4/tarball/ -O ${django}

sudo tar -xzvf ${django}
sudo python Django-1.4/setup.py install

echo "Completed Installation of Dependencies."